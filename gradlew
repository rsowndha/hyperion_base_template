die () {
    echo
    echo "$*"
    echo
    exit 1
}

file="./gradle.properties"
if [ -f "$file" ] ; then
    export $(cat $file | tr -d '\r')
else
    die "$file not found."
fi

if [ -n "$BUILD_SYSTEM_ROOT" ] ; then
    BUILD_SYSTEM_PATH=$BUILD_SYSTEM_ROOT/$buildSystemVersion
else
    BUILD_SYSTEM_PATH=aste_build_system/build-system/$buildSystemVersion
fi

GRADLEW_PATH=$BUILD_SYSTEM_PATH/gradlew
if [ -f "$GRADLEW_PATH" ] ; then
    chmod +x "$GRADLEW_PATH"
    exec "$GRADLEW_PATH" -PbuildSystemPath=$BUILD_SYSTEM_PATH "$@"
else
    die "ERROR: Environment variable "BUILD_SYSTEM_ROOT" is missing and embedded build system is not synced
Please check Build System guide:
https://confluence.jnj.com/display/ABBQ/Hyperion+Handbook#HyperionHandbook-BuildSystem
or
clone this repo recursively: git clone --recursive <git-repository-url>"
fi
