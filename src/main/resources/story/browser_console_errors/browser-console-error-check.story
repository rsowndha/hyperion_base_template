Scenario: Check browser console errors for page
Given I am on the main application page
Then there are no browser console errors
Then there are no browser console errors by regex '.*'

Scenario: Check browser console errors for site
When I crawl the site with URL '${mainApplicationPageUrl}' and for each page do
|step                                                  |
|Then there are no browser console errors              |
|Then there are no browser console errors by regex '.*'|