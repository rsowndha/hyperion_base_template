Meta:
	@layout native

Scenario: Calculation of sum

Given I run the application located at 'http://appium.s3.amazonaws.com/TestApp8.0.app.zip'
When I enter '12' to the field with accessibilityId 'IntegerA'
When I enter '8' to the field with accessibilityId 'IntegerB'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '20'

Scenario: Reset application after relaunch

When I reset the application
Then a field with the name 'IntegerA' exists
Then a field with the name 'IntegerB' exists

Scenario: Failed test example

When I enter '12' to the field with accessibilityId 'IntegerA'
When I enter '8' to the field with accessibilityId 'IntegerB'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '21'

Scenario: Alert handling

When I tap on a button with the name 'show alert'
Then an alert with title 'Cool title' exists
Then the text 'Cool title' exists
Then the text 'this alert is so cool.' exists
When I tap on a button with the name 'OK'
Then the text 'Cool title' does not exist
Then the text 'this alert is so cool.' does not exist
