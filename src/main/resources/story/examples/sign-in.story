Scenario: Janrain Modal Window
Given I am on a page with the URL 'http://www.healthyessentials.com'
When I click on a link with the text 'Sign in'
Then the page contains an element with the name 'janrainModal'
Then an element with the name 'janrainModal' contains a button with the name 'Sign In'
Then an element with the name 'janrainModal' contains a button with the name 'Sign Up'