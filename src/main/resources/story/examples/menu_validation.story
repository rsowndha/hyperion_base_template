Scenario: Menu Validation
Given I am on a page with the URL 'https://www.rocskincare.com/'
When I change context to a level-1 menu with the name 'main-menu'
Then context contains list of link items with the text and link:
|text          |link                 |
|PRODUCTS      |/products            |
|SKIN CONCERNS |/fine-lines-wrinkles |
|ABOUT RETINOL |/retinol             |
|ABOUT HEXINOL�|/what-is-hexinol     |
|ABOUT RoC�    |/about-roc-skin-care |
|WHERE TO BUY  |/where-to-buy        |
Then a level-1 menu contains a level-2 menu with the name 'PRODUCTS' and items with text and link:
|text                           |link                                           |
|ALL PRODUCTS                   |/products                                      |
|DAILY MOISTURIZERS WITH SPF    |/products?field_product_category_tid%5B%5D=17  |
|NIGHT CREAMS                   |/products?field_product_category_tid%5B%5D=18  |
|EYE CREAMS                     |/products?field_product_category_tid%5B%5D=19  |
|CLEANSERS & EXFOLIATORS        |/products?field_product_category_tid%5B%5D=20  |
|SERUMS, SMOOTHERS & PRIMERS    |/products?field_product_category_tid%5B%5D=21  |
|DEEP WRINKLE TREATMENTS        |/products?field_product_category_tid%5B%5D=22  |
