GivenStories: story/precondition/OpenMainApplicationPage.story

Scenario: Check resources from the application
When I crawl the site with URL '${mainApplicationPageUrl}' and for each page do
|step                                                 |
|Then all links are clickable                         |
|Then all resources of types are valid: ${resourceTypes}|

Scenario: Check links from the application
Meta: @linkChecker
When I crawl the site with URL '${mainApplicationPageUrl}' and for each page do 
|step                        			    |
|Then all resources of types are valid: LINK|
