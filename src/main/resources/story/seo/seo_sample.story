Scenario: Robots.txt specified bot testing by all rules
Then robots.txt file is valid for '<bot>' robot type
Examples:
|bot           |
|DEFAULT       |
|ANY           |
|GOOGLE_BOT    |

Scenario: Sitemap testing
Then the application sitemap is valid

Scenario: All SEO parameters for the opened page testing
Given I am on the main application page
Then I check all SEO parameters for the opened page

Scenario: All SEO parameters for the application testing
When I crawl the site with URL '${mainApplicationPageUrl}' and for each page do
|step                                               |
|Then I check all SEO parameters for the opened page|

Scenario: Soft 404 error pages for the application testing
When I crawl the site with URL '${mainApplicationPageUrl}' and for each page do
|step                                               |
|Then the opened page is not soft 404 error page customized by element with XPath '//h1[text()='Page not found']'|
