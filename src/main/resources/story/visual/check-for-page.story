Meta:
    @group Visual Tests
    @layout desktop tablet phone

Scenario: Check Page Against Production Baseline
Given I am on a page with the URL '${targetSiteUrl}'
When I compare the opened page against the visual baseline with name '${baselineName}'
