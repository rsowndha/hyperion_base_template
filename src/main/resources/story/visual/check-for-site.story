Meta:
    @group Visual Tests
    @layout desktop tablet phone

Scenario: Check Site Against Production Baseline
When I crawl the site with URL '${targetSiteUrl}' and for each page do
|step |
|When I compare the opened page against the visual baseline with name '${baselineName}'|
