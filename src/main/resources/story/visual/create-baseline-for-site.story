Meta:
    @group Visual Tests
    @layout desktop tablet phone

Scenario: Establish Site Baseline
When I crawl the site with URL '${baselineSiteUrl}' and for each page do
|step |
|When I establish a visual baseline with name '${baselineName}' for the opened page|
