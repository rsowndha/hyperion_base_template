Meta:
    @group Visual Tests
    @layout desktop tablet phone

Scenario: Establish Page Baseline
Given I am on a page with the URL '${baselineSiteUrl}'
When I establish a visual baseline with name '${baselineName}' for the opened page
