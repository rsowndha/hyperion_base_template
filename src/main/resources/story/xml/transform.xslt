<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">
	<xsl:output method="xml" omit-xml-declaration="yes" />
	<xsl:template match="/response">
		<html>
			<head>
				<title></title>
			</head>
			<body>
				<p>
					<xsl:value-of select="number" />
				</p>
				<p>
					<xsl:value-of select="text" />
				</p>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>