Scenario: Check page accessibility
Given I am on the main application page
Then I check the accessibility of the opened page

Scenario: Check application accessibility
When I crawl the site with URL '${mainApplicationPageUrl}' and for each page do
|step                                             |
|Then I check the accessibility of the opened page|
