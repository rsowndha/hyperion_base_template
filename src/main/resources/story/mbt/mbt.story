Scenario: Open_main_application_page
Meta:
    @layout desktop phone tablet
Given I am on the main application page

Scenario: Open_products_page
Meta:
    @layout desktop phone tablet
When I go to the relative URL '/products'

Scenario: Open_contact_us_page
Meta:
    @layout desktop phone tablet
When I go to the relative URL '/contact-us'

Scenario: Products_page_is_opened
Meta:
    @layout desktop phone tablet
Then the page has the relative URL '/products'

Scenario: Homepage_is_opened
Meta:
    @layout desktop phone tablet
Then the page has the relative URL '/'

Scenario: Contact_us_page_is_opened
Meta:
    @layout desktop phone tablet
Then the page has the relative URL '/contact-us'