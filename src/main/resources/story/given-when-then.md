#Introduction
Given-When-Then's are areas that you can break apart your tests and provide a readable context with regards to the state of the environment, what you intend to do within that given environment and the resulting behavior or state based on what you've done.

##Given
This is usually a statement that sets up your test. This is where we define the starting point in order for us to execute some actions.
##When
These are the actions that we perform. Actions are not limited to functional actions. These can be non-functional actions as well (i.e. http requests initiated by a timed script)
##Then
These are the changes or resulting states that happen after we perform given actions (When) based on a given context (Given)

#To-Do
##Examples