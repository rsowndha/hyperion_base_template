Meta:
    @3rdParty analitics

Scenario: Check analytics metatags
Meta:
    @integration
    @testTier 1
    @testType UI
    @proxy
    
Given I am on the main application page
When I clear the proxy log
And I go to the relative URL '<relativeURL>'
Then I capture an analytics call
And values of the attribute 'lang' of the tag 'html' and a call parameter with the name 'v34' are equal
And values of a meta tag with the name 'pagename' and a call parameter with the name 'pageName' are equal
And values of a meta tag with the name 'pagename' and a call parameter with the name 'c14' are equal
And values of a meta tag with the name 'pagename' and a call parameter with the name 'v16' are equal
And values of a meta tag with the name 'pagename' and a call parameter with the name 'v17' are equal
And values of a meta tag with the name 'sitesection' and a call parameter with the name 'ch' are equal
And values of a meta tag with the name 'pagetype' and a call parameter with the name 'v18' are equal
And values of a meta tag with the name 'platformVersion' and a call parameter with the name 'c46' are equal
And the analytics call contains the parameter 'v14' with the value 'D=ch'
And the page code contains an element with the tag 'meta' and attribute 'name'='subsection'
And the page code contains an element with the tag 'meta' and attributes:
|attributeName  |attributeValue |
|name           |<attrName>     |
|content        |<attrValue>    |

Examples:
|relativeURL                                    |<attrName> |<attrValue>|
|/                                              |pagetype   |home               |
|/search/site/sea                               |pagetype   |search             |
|/contact-us                                    |pagetype   |contact            |
|/products                                      |pagetype   |product category   |
|/product-categories                            |pagetype   |product category   |
|/product-categories/product-category-1         |pagetype   |product category   |
|/products/product-category-1/sprint-27-product |pagetype   |product detail     |
|/coupons                                       |pagetype   |special offers     |
|/where-to-buy                                  |pagetype   |product finder     |
|/111223                                        |pagetype   |error page         |
|/sitemap                                       |pagetype   |sitemap            |
|/legal                                         |pagetype   |legal              |
|/faq                                           |pagetype   |faq                |
|/privacy                                       |pagetype   |information        |
