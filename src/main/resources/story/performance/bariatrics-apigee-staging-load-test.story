
Scenario: Generate access token
When I issue a HTTP GET request for a resource with the URL 'https://jnj-staging.apigee.net/headless/auth/app'
When I save a JSON element from response by JSON path 'app-token' to story variable 'appToken'
Given request body: 
mail=#{P(yyMMddhhmmss)}@mail.com&firstName=Autotest&password=Password1
When I set request headers:
|name                     |value                                             |
|Authorization            |Bearer #{removeWrappingDoubleQuotes(${appToken})} |
|appName                  |hws.healthpartner.weightloss.mobile               |
|Content-Type             |application/x-www-form-urlencoded                 |
When I issue a HTTP POST request for a resource with the URL 'https://jnj-staging.apigee.net/v1/health-partners-microservice/user/registration'
When I save a JSON element from response by JSON path 'access_token' to story variable 'accessToken'

Scenario: retrieve entity ids for test
When I set request headers:
|name                     |value                                                |
|Authorization            |Bearer #{removeWrappingDoubleQuotes(${accessToken})} |
|appName                  |hws.healthpartner.weightloss.mobile                  |
When I issue a HTTP GET request for a resource with the URL 'https://jnj-staging.apigee.net/bariatrics-content/user-action-entity-list'
When I save a JSON element from response by JSON path '[0].id' to story variable 'entityId1'
When I save a JSON element from response by JSON path '[1].id' to story variable 'entityId2'

Scenario: Load test
Given I add sample requests with parameters:
|relativeUrl                                     |
|/bariatrics-content/learning_center/detail/2971 |
|/bariatrics-content/learning_center/filters     |
|/bariatrics-content/learning_center/list        |
|/bariatrics-content/application-type            |
|/bariatrics-content/goals/get-goals             |
|/bariatrics-content/headless/get/static         |
|/bariatrics-content/user-action-entity-list     |
|/bariatrics-content/user_management/get-profile |
Given I add sample requests with parameters:
|relativeUrl                       |httpMethod|
|/bariatrics-content/bookmarks/get |POST      |
Given I add sample requests with parameters:
|relativeUrl                       |httpMethod|headerName1|headerValue1|
|/bariatrics-content/bookmarks/is  |POST      |nid        |2981        |
|/bariatrics-content/bookmarks/add |POST      |nid        |2981        |
Given I add sample requests with parameters:
|relativeUrl                                    |httpMethod|requestBody|
|/bariatrics-content/user-action-entity-details |POST      |{"id": ${entityId1}}  |
|/bariatrics-content/user-action-entity-related |POST      |{"id": ${entityId2}}  |
Given I set sample request headers:
|name          |value                                                |
|Authorization |Bearer #{removeWrappingDoubleQuotes(${accessToken})} |
|appName       |hws.healthpartner.weightloss.mobile                  |
Given I set user interaction timer with delay 50 and range 300
When I run load test with the name 'bariatrics-staging-apigee-automated-load-test-demo' and parameters:
|threads|duration|rampup|iterations|url                            |overrideConfiguration|
|10     |5       |100   |-1        |https://jnj-staging.apigee.net |true                 |
Then the average response time should be less than '1000' milliseconds
Then the TP90 response time should be less than '1500' milliseconds
Then the total amount of failures should be less than 1 percents
