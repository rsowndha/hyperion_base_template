Meta:
	@platformName Windows 7
	@browserName firefox
	@version 37.0

Scenario: Firefox Tablet Layout Screenshot
Meta:
	@screen-resolution 1024x768
Given I am on the main application page
When I take a screenshot to 'screenshots/Firefox/Tablet.png'

Scenario: Firefox Desktop Layout Screenshot
Meta:
	@screen-resolution 1152x864
Given I am on the main application page
When I take a screenshot to 'screenshots/Firefox/Desktop.png'

Scenario: Firefox Large Desktop Layout Screenshot
Meta:
	@screen-resolution 1920x1200
Given I am on the main application page
When I take a screenshot to 'screenshots/Firefox/Desktop-Large.png'
