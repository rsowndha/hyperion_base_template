Meta:
    @platformName android
	@platformVersion 7.0
	@browserName chrome
	@screen-resolution 1024x768
	
Scenario: Android Phone Portrait Screenshot
Meta:
	@deviceName Android GoogleAPI Emulator
	@device-orientation portrait
Given I am on the main application page
When I take a screenshot to 'screenshots/Android/Phone-Portrait.png'

Scenario: Android Phone Landscape Screenshot
Meta:
	@deviceName Android GoogleAPI Emulator
	@device-orientation landscape
Given I am on the main application page
When I take a screenshot to 'screenshots/Android/Phone-Landscape.png'
