Meta:
	@platformName Windows 7
	@browserName chrome
	@version 41.0

Scenario: Chrome Tablet Layout Screenshot
Meta:
	@screen-resolution 1024x768
Given I am on the main application page
When I take a screenshot to 'screenshots/Chrome/Tablet.png'

Scenario: Chrome Desktop Layout Screenshot
Meta:
	@screen-resolution 1152x864
Given I am on the main application page
When I take a screenshot to 'screenshots/Chrome/Desktop.png'

Scenario: Chrome Large Desktop Layout Screenshot
Meta:
	@screen-resolution 1920x1200
Given I am on the main application page
When I take a screenshot to 'screenshots/Chrome/Desktop-Large.png'
