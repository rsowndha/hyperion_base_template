Meta:
	@platformName OS X 10.13
	@browserName safari
	@version 11.1

Scenario: Safari Tablet Layout Screenshot
Meta:
	@screen-resolution 1024x768
Given I am on the main application page
When I take a screenshot to 'screenshots/Safari/Tablet.png'

Scenario: Safari Desktop Layout Screenshot
Meta:
	@screen-resolution 1152x864
Given I am on the main application page
When I take a screenshot to 'screenshots/Safari/Desktop.png'

Scenario: Safari Large Desktop Layout Screenshot
Meta:
	@screen-resolution 1920x1440
Given I am on the main application page
When I take a screenshot to 'screenshots/Safari/Desktop-Large.png'
