Meta:
	@platformName Windows 7
	@browserName internet explorer
	@version 11.0
	@iedriver-version x64_2.45.0

Scenario: IE Tablet Layout Screenshot
Meta:
	@screen-resolution 1024x768
Given I am on the main application page
When I take a screenshot to 'screenshots/IE/Tablet.png'

Scenario: IE Desktop Layout Screenshot
Meta:
	@screen-resolution 1152x864
Given I am on the main application page
When I take a screenshot to 'screenshots/IE/Desktop.png'

Scenario: IE Large Desktop Layout Screenshot
Meta:
	@screen-resolution 1920x1200
Given I am on the main application page
When I take a screenshot to 'screenshots/IE/Desktop-Large.png'
