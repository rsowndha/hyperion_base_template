Meta:
    @platformName iOS
	@platformVersion 11.3
	@browserName safari
	@screen-resolution 1024x768
	
Scenario: iPhone Portrait Screenshot
Meta:
	@deviceName iPhone Simulator
	@device-orientation portrait
Given I am on the main application page
When I take a screenshot to 'screenshots/iOS/iPhone-Portrait.png'

Scenario: iPhone Landscape Screenshot
Meta:
	@deviceName iPhone Simulator
	@device-orientation landscape
Given I am on the main application page
When I take a screenshot to 'screenshots/iOS/iPhone-Landscape.png'

Scenario: iPad Portrait Screenshot
Meta:
	@deviceName iPad Simulator
	@device-orientation portrait
Given I am on the main application page
When I take a screenshot to 'screenshots/iOS/iPad-Portrait.png'

Scenario: iPad Landscape Screenshot
Meta:
	@deviceName iPad Simulator
	@device-orientation landscape
Given I am on the main application page
When I take a screenshot to 'screenshots/iOS/iPad-Landscape.png'
