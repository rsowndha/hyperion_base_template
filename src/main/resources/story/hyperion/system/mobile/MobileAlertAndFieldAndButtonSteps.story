Meta:
    @layout native
    @group bdd-steps-mobile
    @skip

GivenStories: story/hyperion/system/mobile/precondition/OpenApplication.story


Scenario: MobileAlertSteps - Step verification "Then an alert with title '$title' exists"
When I tap on an element with the name 'show alert'
Then an alert with title 'Cool title' exists
When I tap on a button with the name 'OK'


Scenario: MobileFieldSteps - Step verification "When I enter '$text' to the field with accessibilityId '$accessibilityId'"
When I enter '7' to the field with accessibilityId 'IntegerA'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '7'


Scenario: MobileFieldSteps - Step verification "When I enter '$text' in a field with the name '$fieldName'"
When I enter '8' in a field with the name 'IntegerB'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '15'


Scenario: MobileFieldSteps - Step verification "When I enter '$text' in a field by the xpath '$xpath'"
When I enter '3' in a field by the xpath '//XCUIElementTypeTextField[@name="IntegerA"]'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '11'


Scenario: MobileFieldSteps - Step verification "When I clear field by the xpath '$xpath'"
When I clear field by the xpath '//XCUIElementTypeTextField[@name="IntegerA"]'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '8'


Scenario: MobileFieldSteps - Step verification "Then a field with the name '$fieldName' and text '$text' exists"
Then a field with the name 'IntegerB' and text '8' exists


Scenario: MobileFieldSteps - Step verification fields's existance
Then a field with the name 'IntegerA' exists
When I tap on a button with the name 'show alert'
Then a field with the name 'IntegerA' does not exist
When I tap on a button with the name 'OK'


Scenario: MobileButtonSteps - Step verification "When I tap on a button with the name '$buttonName'"
When I enter '1' to the field with accessibilityId 'IntegerA'
When I clear field by the xpath '//XCUIElementTypeTextField[@name="IntegerB"]'
When I tap on a button with the name 'ComputeSumButton'
Then an element with accessibilityId 'Answer' contains text '1'


Scenario: MobileButtonSteps - Step verification "When I tap on a button with the name '$buttonName' and wait for button disappearance"
When I tap on a button with the name 'Done' and wait for button disappearance
Then a button with the name 'Done' does not exist
When I hide keyboard


Scenario: MobileButtonSteps - Step verification "Then a button with the name '$buttonName' is '$state'"
Then a button with the name '<buttonName>' is '<state>'
Examples:
|buttonName         |state      |
|show alert         |ENABLED    |
|DisabledButton     |DISABLED   |


Scenario: MobileButtonSteps - Step verification of button's existance
Then a button with the name 'ComputeSumButton' exists
When I tap on a button with the name 'Test Gesture'
Then a button with the name 'ComputeSumButton' does not exist
