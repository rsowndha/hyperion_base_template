Meta:
    @layout native
    @group bdd-steps-mobile


Scenario: Step verification "Given I run the application"
Given I run the application
Then a button with the name 'ComputeSumButton' exists
When I close the application


Scenario: Step verification "When I launch the application"
When I launch the application
Then a button with the name 'ComputeSumButton' exists
When I close the application
