Meta:
    @layout native
    @group bdd-steps-mobile


Scenario: Step verification "When I run the application in background for $seconds seconds"
When I run the application in background for 5 seconds
Then a button with the name 'ComputeSumButton' exists
When I close the application


Scenario: Step verification "When I close the application"
When I launch the application
Then a button with the name 'ComputeSumButton' exists
When I close the application
Then a button with the name 'ComputeSumButton' does not exist
When I close the application