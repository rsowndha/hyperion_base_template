Meta:
    @layout native
    @group bdd-steps-mobile

GivenStories: story/hyperion/system/mobile/precondition/OpenApplication.story

Lifecycle:
Examples:
|inputFieldName |computeSumButton |answer |
|IntegerA       |ComputeSumButton |Answer |


Scenario: MobileElementSteps - Step verification "Then the value of picker wheel with index '$index' is $comparisonRule '$value'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
Then the value of picker wheel with index '$index' is $comparisonRule '$value'


Scenario: MobileElementSteps - Step verification "When I select an element with the text '$text' in $directionType list view" (Skipped due to lack of functionality in the test application(supported for android))
Meta:
    @skip
When I select an element with the text '$text' in vertical list view


Scenario: MobileElementSteps - Step verification "Then an element with accessibilityId '$accessibilityId' contains text '$text'"
When I enter '7' to the field with accessibilityId '<inputFieldName>'
Then an element with accessibilityId '<inputFieldName>' contains text '7'
When I enter '8' to the field with accessibilityId '<inputFieldName>'
Then an element with accessibilityId '<inputFieldName>' contains text '8'


Scenario: MobileElementSteps - Step verification "Then the text '$text' does not exist"
When I enter '9' to the field with accessibilityId '<inputFieldName>'
When I tap on an element with the name '<computeSumButton>'
When I change context to an element by the xpath '//*[@name='<answer>']'
Then the text '9' exists
When I reset context
When I enter '10' to the field with accessibilityId '<inputFieldName>'
When I tap on an element with the name '<computeSumButton>'
When I change context to an element by the xpath '//*[@name='<answer>']'
Then the text '9' does not exist


Scenario: MobileElementSteps - Step verification "Then the text '$text' exists" - with setting context
When I enter '10' to the field with accessibilityId '<inputFieldName>'
When I change context to an element by the xpath '//*[@name='<inputFieldName>']'
Then the text '10' exists
When I reset context
When I enter '11' to the field with accessibilityId '<inputFieldName>'
When I change context to an element by the xpath '//*[@name='<inputFieldName>']'
Then the text '11' exists


Scenario: MobileElementSteps - Step verification "Then the text '$text' exists" - without setting context
When I enter '12' to the field with accessibilityId '<inputFieldName>'
Then the text '12' exists


Scenario: MobileElementSteps - Step verification "Then an element with the attribute '$attributeType'='$attributeValue' exists"
When I enter '13' to the field with accessibilityId '<inputFieldName>'
Then an element with the attribute 'value'='13' exists
When I enter '14' to the field with accessibilityId '<inputFieldName>'
Then an element with the attribute 'value'='14' exists


Scenario: MobileElementSteps - Step verification "Then an element by the xpath '$xpath' exists"
When I enter '15' to the field with accessibilityId '<inputFieldName>'
Then an element by the xpath '//*[@name='<inputFieldName>' and @value='15']' exists


Scenario: MobileElementSteps - Step verification "Then an element with the name '$name' exists"
When I tap on an element with the name 'show alert'
Then an element with the name 'this alert is so cool.' exists
When I tap on a button with the name 'OK'


Scenario: MobileElementSteps - Step verification "Then the number of elements found by the xpath '$xpath' is $comparisonRule '$quantity'"
When I reset the application
Then the number of elements found by the xpath '//XCUIElementTypeOther' is equal to '8'
