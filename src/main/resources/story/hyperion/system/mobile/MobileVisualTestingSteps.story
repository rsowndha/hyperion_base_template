Meta:
    @layout native
    @group plugin-native-visual

GivenStories: story/hyperion/system/mobile/precondition/OpenApplication.story


Scenario: MobileVisualTestingSteps - Step verification "When I $visualTestingAction baseline with name '$baselineName' using the current screen"
When I establish baseline with name 'hyperion-mobile-regression-testBaseline1' using the current screen
When I compare against baseline with name 'hyperion-mobile-regression-testBaseline1' using the current screen


Scenario: MobileVisualTestingSteps - Step verification "When I compare the current screen against the visual baseline with name '$baselineName'"
When I establish a visual baseline with name 'hyperion-mobile-regression-testBaseline2' for current screen
When I compare the current screen against the visual baseline with name 'hyperion-mobile-regression-testBaseline2'


Scenario: MobileVisualTestingSteps - Step verification "When I compare the current screen against the visual baseline with name '$baselineName' with match level '$matchLevel'"
When I establish a visual baseline with name 'hyperion-mobile-regression-testMatchLevelBaseline' for current screen
When I compare the current screen against the visual baseline with name 'hyperion-mobile-regression-testMatchLevelBaseline' with match level 'STRICT'


Scenario: MobileVisualTestingSteps - Step verification "When I $visualTestingAction baseline with name '$baselineName' using the search context"
When I change context to an element by the xpath '//XCUIElementTypeSlider'
When I establish baseline with name 'hyperion-mobile-regression-contextBaseline' using the search context
When I compare against baseline with name 'hyperion-mobile-regression-contextBaseline' using the search context
