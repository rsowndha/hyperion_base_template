Meta:
    @layout native
    @group bdd-steps-mobile

GivenStories: story/hyperion/system/mobile/precondition/OpenApplication.story


Scenario: HybridMobileSteps - Step verification "When I swipe '$swipeDirection'"
When I tap on an element with the name 'Test Gesture'
When I wait until an element with the xpath '//*[@name='North America']' appears
When I swipe 'left'
Then the text 'North America' does not exist


Scenario: HybridMobileSteps - Step verification "When I hide keyboard"
When I reset the application
When I tap on an element with the name 'IntegerA'
Then the text 'Done' exists
When I hide keyboard
When I wait until an element with the xpath '//*[@name='Done']' disappears
Then the text 'Done' does not exist


Scenario: HybridMobileSteps - Step verification "When I change screen orientation to '$screenOrientation'"
Then the number of elements found by the xpath '//XCUIElementTypeWindow[@height>600]' is GREATER_THAN '0'
When I change screen orientation to 'landscape'
Then the number of elements found by the xpath '//XCUIElementTypeWindow[@height>600]' is EQUAL_TO '0'
