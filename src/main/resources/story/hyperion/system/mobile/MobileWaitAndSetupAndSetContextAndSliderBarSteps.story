Meta:
    @layout native
    @group bdd-steps-mobile

GivenStories: story/hyperion/system/mobile/precondition/OpenApplication.story

Lifecycle:
Examples:
|searchItemXpath           |itemNumberInStatusBar |
|//XCUIElementTypeOther    |6                     |


Scenario: WaitSteps - Step verification "Then an element with the xpath '$xpath' exists for '$seconds' seconds"
Then an element with the xpath '//XCUIElementTypeTextField[@name="IntegerA"]' exists for '3' seconds


Scenario: WaitSteps - Step verification "When I wait until an element with the xpath '$xpath' disappears"
When I tap on an element by the xpath '//XCUIElementTypeButton[@name="Test Gesture"]'
When I wait until an element with the xpath '//XCUIElementTypeTextField[@name="IntegerA"]' disappears
When I reset the application


Scenario: WaitSteps - Step verification "When I wait until an element with the xpath '$xpath' appears"
When I wait until an element with the xpath '//XCUIElementTypeTextField[@name="IntegerA"]' appears
Then a field with the name 'IntegerA' exists


Scenario: MobileSetupSteps - Precondition that set up context
When I change context to an element by the xpath '//XCUIElementTypeStatusBar'
Then the number of elements found by the xpath '<searchItemXpath>' is equal to '<itemNumberInStatusBar>'


Scenario: MobileSetupSteps - Step verification @AfterScenario
Then the number of elements found by the xpath '<searchItemXpath>' is greater than '<itemNumberInStatusBar>'


Scenario: SetContextSteps - Step verification "When I change context to an element by the xpath '$xpath'"
When I change context to an element by the xpath '//XCUIElementTypeStatusBar'
Then the number of elements found by the xpath '<searchItemXpath>' is equal to '<itemNumberInStatusBar>'


Scenario: SetContextSteps - Step verification "When I reset context"
When I change context to an element by the xpath '//XCUIElementTypeStatusBar'
When I reset context
Then the number of elements found by the xpath '<searchItemXpath>' is greater than '<itemNumberInStatusBar>'


Scenario: MobileSliderBarSteps - Step verification "When I move slider bar to '$percent' percent"
When I move slider bar to '30' percent
When I set value of attribute 'value' of the element by xpath '//XCUIElementTypeSlider' to the 'scenario' variable 'sliderValue'
When I initialize the scenario variable 'sliderValue' with value '#{replaceFirstByRegExp((\d+)%, $1, ${sliderValue})}'
!-- The adjustment is a “best effort” to move the indicator to the desired position; absolute fidelity is not guaranteed.
!-- https://developer.apple.com/documentation/xctest/xcuielement/1501022-adjust
Then '${sliderValue}' is greater than or equal to '27'
Then '${sliderValue}' is less than or equal to '33'


Scenario: MobileSliderBarSteps - Step verification "When I move slider bar with index '$index' to '$percent' percent"
When I move slider bar with index '1' to '100' percent
When I set value of attribute 'value' of the element by xpath '//XCUIElementTypeSlider' to the 'scenario' variable 'sliderValue'
When I initialize the scenario variable 'sliderValue' with value '#{replaceFirstByRegExp((\d+)%, $1, ${sliderValue})}'
!-- The adjustment is a “best effort” to move the indicator to the desired position; absolute fidelity is not guaranteed.
!-- https://developer.apple.com/documentation/xctest/xcuielement/1501022-adjust
Then '${sliderValue}' is greater than or equal to '97'
