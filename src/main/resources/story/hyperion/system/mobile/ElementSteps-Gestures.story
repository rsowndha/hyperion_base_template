Meta:
    @layout native
    @group bdd-steps-mobile

GivenStories: story/hyperion/system/mobile/precondition/OpenApplication.story

Lifecycle:
Examples:
|inputFieldName |computeSumButton |answer |torontoXpath         |
|IntegerA       |ComputeSumButton |Answer |//*[@name='Toronto'] |


Scenario: MobileElementSteps - Step verification "When I tap on an element with the text '$text'"
When I enter '1' to the field with accessibilityId '<inputFieldName>'
When I tap on an element with the text 'Compute Sum'
Then an element with accessibilityId '<answer>' contains text '1'


Scenario: MobileElementSteps - Step verification "When I tap on an element by the xpath '$xpath'"
When I enter '2' to the field with accessibilityId '<inputFieldName>'
When I tap on an element by the xpath '//*[@name='<computeSumButton>']'
Then an element with accessibilityId '<answer>' contains text '2'


Scenario: MobileElementSteps - Step verification "When I tap on elements by the xpath '$xpath'"
When I enter '3' to the field with accessibilityId '<inputFieldName>'
When I tap on elements by the xpath '//*[@name='<computeSumButton>' or @name='show alert']'
When I tap on a button with the name 'OK'
Then an element with accessibilityId '<answer>' contains text '3'


Scenario: MobileElementSteps - Step verification "When I tap outside an element with the xpath '$xpath'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
When I tap outside an element with the xpath '$xpath'


Scenario: MobileElementSteps - Step verification "When I tap on an element with the name '$name'"
When I enter '4' to the field with accessibilityId '<inputFieldName>'
When I tap on an element with the name '<computeSumButton>'
Then an element with accessibilityId '<answer>' contains text '4'
When I hide keyboard


Scenario: MobileElementSteps - Step verification "When I swipe '$swipeDirection' on element with accessibilityId '$accessibilityId'" (Steps values configured for running under SauceLabs)
When I tap on a button with the name 'Test Gesture'
When I wait until an element with the xpath '<torontoXpath>' appears
When I swipe 'up' on element with accessibilityId 'Toronto'
When I wait until an element with the xpath '<torontoXpath>' disappears
When I reset the application


Scenario: MobileElementSteps - Step verification "When I swipe to an element with the xpath '$xpath'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
When I swipe to an element with the xpath '$xpath'


Scenario: MobileElementSteps - Step verification "When I swipe '$swipeDirection' an element by the xpath '$xpath'" (Steps values configured for running under SauceLabs)
When I tap on a button with the name 'Test Gesture'
When I wait until an element with the xpath '<torontoXpath>' appears
When I swipe 'up' an element by the xpath '<torontoXpath>'
When I wait until an element with the xpath '<torontoXpath>' disappears
When I reset the application


Scenario: MobileElementSteps - Step verification "When I pinch element with accessibilityId '$accessibilityId'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
When I pinch element with accessibilityId '$accessibilityId'


Scenario: MobileElementSteps - Step verification "When I zoom element with accessibilityId '$accessibilityId'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
When I zoom element with accessibilityId '$accessibilityId'


Scenario: MobileElementSteps - Step verification "When I pick '$text' on picker wheel with index '$index'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
When I pick '$text' on picker wheel with index '$index'


Scenario: MobileElementSteps - Step verification "When I try to pick '$text' on picker wheel with index '$index'" (Skipped due to lack of functionality in the test application)
Meta:
    @skip
When I try to pick '$text' on picker wheel with index '$index'
