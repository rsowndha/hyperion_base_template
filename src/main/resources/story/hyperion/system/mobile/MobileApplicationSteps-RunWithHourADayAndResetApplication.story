Meta:
    @layout native
    @group bdd-steps-mobile


Scenario: Step verification "Given I run application with hour of day set to $hourOfDay"
Given I run application with hour of day set to 2
Then a button with the name 'ComputeSumButton' exists
When I close the application


Scenario: Step verification "When I reset the application"
When I reset the application
Then a button with the name 'ComputeSumButton' exists
When I close the application
