Meta:
    @group visual-testing
    @layout desktop
    @sauceConnect


Scenario: Step verification I $visualTestingAction baseline with name '$baselineEnvName'
Given I am on the main application page
When I establish baseline with name 'hyperion-desktop-regression-testBaseline-1'
When I compare the opened page against the visual baseline with name 'hyperion-desktop-regression-testBaseline-1'

Scenario: Step verification I establish a visual baseline with name '$baselineEnvName' for the opened page
Given I am on the main application page
When I click on a link with the URL containing '/html/page.html' and the text 'This is a link'
When I establish a visual baseline with name 'hyperion-desktop-regression-testBaseline-2' for the opened page
When I compare the opened page against the visual baseline with name 'hyperion-desktop-regression-testBaseline-2'

Scenario: Step verification I compare the opened page against the visual baseline with name '$baselineEnvName' with match level '$matchLevel'
Given I am on the main application page
When I click on a link with the URL containing '/html/page.html' and the text 'This is a link'
When I establish a visual baseline with name 'hyperion-desktop-regression-testBaseline-3' for the opened page
When I compare the opened page against the visual baseline with name 'hyperion-desktop-regression-testBaseline-3' with match level 'LAYOUT'

Scenario: Step verification I $visualTestingAction baseline with name '$baselineEnvName' using the search context
Given I am on the main application page
When I change context to an element by the xpath '//div[@id='navbar']'
When I establish baseline with name 'hyperion-desktop-regression-testBaseline-4'
When I compare against baseline with name 'hyperion-desktop-regression-testBaseline-4'

Scenario: Step verification I $visualTestingAction baseline with name '$baselineEnvName' $parameters
Given I am on the main application page
When I establish baseline with name 'hyperion-desktop-regression-testBaseline-5'
|ignoredElementAreaLocators       |ignoredPageAreaLocators          |
|By.xpath(.//*[@id="my-carousel"])|                                 |
|                                 |By.xpath(.//*[@id="my-carousel"])|
When I compare against baseline with name 'hyperion-desktop-regression-testBaseline-5'
|ignoredElementAreaLocators       |ignoredPageAreaLocators          |
|By.xpath(.//*[@id="my-carousel"])|                                 |
|                                 |By.xpath(.//*[@id="my-carousel"])|

Scenario: Step verification I $visualTestingAction baseline with name '$baselineEnvName' with context element
Given I am on the main application page
When I change context to an element by the xpath '//div[@class='image-to-disappear']'
When I establish baseline with name 'hyperion-desktop-regression-testBaseline-6'
When I click on an element by the xpath '//div[@id='disappear-element']/button'
When I compare not equal to baseline with name 'hyperion-desktop-regression-testBaseline-6'
