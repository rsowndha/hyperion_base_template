Meta:
	@proxy
	@browserName CHROME

Scenario: Verification of SauceLabs and Applitools integration
Given I am on the main application page
Then the page with the URL '${mainPageURL}' is loaded
When I establish baseline with name 'DTE-HYPERION-SYSTEM-TEST'
