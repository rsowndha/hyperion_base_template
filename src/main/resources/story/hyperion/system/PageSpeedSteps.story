Scenario: Verification of Google PageSpeed Score integration
Then the pagespeed score of the URL 'http://example.com/' > '80'

Scenario: Verification of Google PageSpeed Score integration for mobile strategy
Then the pagespeed score of the URL 'http://example.com/' > '80' on 'mobile'