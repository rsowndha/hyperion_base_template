Scenario: Verifications of SSLLabs steps
When I perform an SSL Scan for URL 'https://google.com'
Then the SSL rating for URL 'https://google.com' is > 'M'
