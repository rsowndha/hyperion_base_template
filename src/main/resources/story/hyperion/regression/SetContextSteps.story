Meta:
    @group plugin-web-ui
    @testSetId AAMH-5627

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I change context to the page
Meta:
    @testCaseId AAMH-3778
When I change context to an element by the xpath '//table[@id="three-rows-table"]'
Then the text '<oneColumnTableText>' does not exist
When I change context to the page
Then the text '<oneColumnTableText>' exists
Examples:
|oneColumnTableText|
|1 Column:|

Scenario: Step verification When I change context to an element with the attribute=value
Meta:
    @testCaseId AAMH-3779
When I change context to an element by the xpath '//table[@id="three-rows-table"]'
Then the text '<linkText>' does not exist
When I change context to an element with the attribute 'id'='button-container'
Then the text '<linkText>' exists
Examples:
|linkText|
|This is a link|

Scenario: Step verification When I switch to a frame with the attribute
Meta:
    @testCaseId AAMH-3780
When I go to the relative URL '/html/pageOther.html'
When I change context to an element with the attribute 'id'='frame-for-check'
Then the text '<myIFrame>' does not exist
When I click on a button with the name 'Begin Editing'
And I switch to a frame with the attribute 'id'='myIframe'
Then the text '<myIFrame>' exists
Examples:
|myIFrame|
|test.html|

Scenario: Step verification When I switch back to the page
Meta:
    @testCaseId AAMH-3781
When I switch to a frame with the attribute 'id'='myIframe'
Then the text '<frameText>' exists
When I switch back to the page
Then the text '<frameText>' does not exist
Examples:
|frameText|
|test.html|

Scenario: Step verification When I change context to an element with the name
Meta:
    @testCaseId AAMH-3782
When I go to the relative URL '/'
When I change context to an element by the xpath '//div[@id='disappear-element']'
Then the text '<some_text>' does not exist
When I change context to an element with the name '<some_text>'
Then the text '<some_text>' exists
Examples:
|some_text     |
|This is a link|

Scenario: When I change context to the row '$rowNumber' in a table with the attribute '$attributeType'='$attributeValue'
Meta:
    @testCaseId AAMH-3783
When I change context to the row '<row>' in a table with the attribute '<type>'='<value>'
Then the text '<some_text>' does not exist
When I change context to an element by the xpath '//div[@custom-attr='scrollbar']'
Then the text '<some_text>' exists
Examples:
|row |type |value            |some_text |
|1   |id   |three-rows-table |Scrollbar |

Scenario: Verification When I change context to a pop-up
Meta:
    @testCaseId AAMH-3784
When I go to the relative URL '/html/page.html'
When I change context to a pop-up
Then an element with the name 'Pop-up' does not exist
Then an element with the name 'Z-Index=0' exists

Scenario: Step verification When I switch to a window with the name '$windowName'
Meta:
    @testCaseId AAMH-3785
When I go to the relative URL '/'
When I click on an element with the text 'OpenNewWindow'
When I switch to a window with the name 'Hyperion Template - Other Page'
Then the page title is 'Hyperion Template - Other Page'
When I close the current window
When I switch to a window with the name 'Hyperion Template'
Then the page title is 'Hyperion Template'