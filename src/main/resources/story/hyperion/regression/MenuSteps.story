Meta:
    @group plugin-web-ui
    @testSetId AAMH-5617

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Go to the relative URL
Meta:
    @testCaseId AAMH-3749
When I go to the relative URL '/html/pageOther.html'

Scenario: Step verification When I hover a mouse over a menu item with the name
Meta:
    @testCaseId AAMH-3750
Then an element with the name '<elementText>' exists
When I hover a mouse over a menu item with the name '<elementText>'
Then an element with the name '<elementText>' does not exist
Examples:
|elementText            |
|Go to the main page    |

Scenario: Step verification Then a level-1 menu contains
Meta:
    @testCaseId AAMH-3751
Then a level-1 menu contains a level-3 menu and does not contain a level-4 menu
When I change context to an element by the xpath '//nav[@class="main-menu"]'
When I click on an element with the text 'Menu Dropdown'
Then a level-1 menu contains a level-2 menu with the name 'Menu Dropdown' and items with text and link:
|text                           |link                     |
|Page                   		|page.html                |


