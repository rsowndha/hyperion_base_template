Meta:
    @group plugin-web-ui
    @testSetId AAMH-5605

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification that an element exists or does not exist
Meta:
    @testCaseId AAMH-3639
Then an element with the name '<buttonNameClick>' exists
Then an element with the name 'wrongName' does not exist
Then at least one element with the attribute 'type'='radio' exists
Then an element by the xpath '//input[@type="radio"][@value="female"]' exists
Then an element by the cssSelector '.navbar-brand' exists
Then at least one element by the xpath '//input[@type="radio"]' exists
!-- Skipped due to jbrowser bug: https://www.jjconsumer.com/jira/browse/DTE-4015: Then an element with the tag 'textarea' and text 'The cat was playing in the garden.' exists
Then an element with the tag 'p' and text 'Some text in a p tag' exists
Then an element with the attribute '<typeAttribute>' containing 'with-image' exists
Then an element with the tag 'button' and attribute '<typeAttribute>'='<valueAttribute>' exists
Then an element with the attribute '<typeAttribute>'='<valueAttribute>' exists
Then an element with the name '<valueAttribute>' and text '<buttonText>' exists
Then a [VISIBLE] element with the name '<buttonNameClick>' exists
Then a [VISIBLE] element with the attribute '<typeAttribute>'='<valueAttribute>' exists
Then a [VISIBLE] element with the name '<valueAttribute>' and text '<buttonText>' exists
Examples:
|buttonNameClick     |typeAttribute  |valueAttribute    |buttonText      |
|Click Me!           |name           |button-with-image |Button with img |

Scenario: Step verification Then an element contains the radio buttons
Meta:
    @testCaseId AAMH-3640
Then an element contains the radio buttons:
|radioOption  |
|Male         |
|Female       |
|Other        |

Scenario: Step verification When I click on an element by the xpath
Meta:
    @testCaseId AAMH-3641
Then a [NOT_SELECTED] radio button with the name '<radioOptionFemale>' exists
When I click on an element by the xpath '//input[@type="radio"][@value="female"]'
Then a [SELECTED] radio button with the name '<radioOptionFemale>' exists
Examples:
|radioOptionFemale  |
|Female             |

Scenario: Step verification When I click on an element with the attribute
Meta:
    @testCaseId AAMH-3642
Then a [NOT_SELECTED] radio button with the name '<radioOptionMale>' exists
When I click on an element with the attribute 'value'='male'
Then a [SELECTED] radio button with the name '<radioOptionMale>' exists
Examples:
|radioOptionMale    |
|Male               |

Scenario: Step verification Then the number of elements found by the xpath is equal
Meta:
    @testCaseId AAMH-3643
Then the number of elements found by the xpath '//img[@src="/img/hyperion.jpg"]' is equal to '2'

Scenario: Step verification When I click on an element with the text
Meta:
    @testCaseId AAMH-3644
Then the page title is 'Hyperion Template'
When I click on an element with the text 'This is a link'
Then the page title is 'Hyperion Template - Other Page'

Scenario: Step verification Then the context has a width
Meta:
    @testCaseId AAMH-3645
When I change context to an element by the xpath '//div[@id="for-link"]'
Then the context has a width of '50'%

Scenario: Step verification Then the context has a width of 'width'% relative to the parent element
Meta:
    @testCaseId AAMH-3646
When I change context to an element by the xpath '//div[@id="img-for-hover"]/a/img'
Then the context element has a width of '41'% relative to the parent element

Scenario: Step verification Then each element by the xpath has the same 'dimension'
Meta:
    @testCaseId AAMH-3647
When I change context to an element by the xpath '//table'
Then each element by the xpath '//table/tbody/tr' has the same 'height'

Scenario: Step verification Then the context element has the CSS property
Meta:
    @testCaseId AAMH-3648
When I change context to an element by the xpath '//table'
Then the context element has the CSS property 'padding'='5px'
Then the context element has the CSS property 'border' containing '1px solid'

Scenario: Step verification When I hover a mouse over an element with the xpath '$xpath'
Meta:
    @testCaseId AAMH-3649
Then an element with the name '<elementText>' exists
When I hover a mouse over an element with the xpath '//div[@id="for-link"]/a'
Then an element with the name '<elementText>' does not exist
Examples:
|elementText            |
|Go to the main page    |

Scenario: Step verification Then each element with the xpath has 'number' child elements with the xpath
Meta:
    @testCaseId AAMH-3650
Then each element with the xpath '//table/tbody' has '3' child elements with the xpath '//table/tbody/tr'

Scenario: Go to the relative URL
Meta:
    @testCaseId AAMH-3651
When I go to the relative URL '/html/pageOther.html'

Scenario: Step verification When I click on an element then the page does not refresh
Meta:
    @testCaseId AAMH-3652
Then an element with the name '<messageElement>' does not exist
When I click on an element with the text 'ButtonWithoutReloadPage' then the page does not refresh
Then an element with the name '<messageElement>' exists
Examples:
|messageElement             |
|Page doesn't refresh       |

Scenario: Step verification When I select an element and upload the file
Meta:
    @testCaseId AAMH-3653
Then an element with the name '<elementText>' does not exist
When I select an element with the 'id'='upload-file' and upload the file 'story/hyperion/upload/2.jpeg'
When I click on a button with the name 'button-show-file'
Then an element with the name '<elementText>' exists
Examples:
|elementText |
|2.jpeg      |

Scenario: Then at least one element by the xpath '$xpath' exists
Meta:
    @testCaseId AAMH-3654
When I change context to an element by the xpath 'footer'
Then at least one element by the xpath '<elementXpath>' exists
Examples:
|elementXpath                        |
|//a[contains(text(), 'Back to top')]|