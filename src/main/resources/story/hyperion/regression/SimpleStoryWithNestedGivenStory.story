Meta:
    @testSetId AAMH-28646

GivenStories: /story/hyperion/regression/precondition/GivenStoryWithGivenStoryOnScenarioLevel.story

Scenario: Simple scenario to be executed after complex given story
Meta:
	@root
	@testCaseId AAMH-20371
Then 'data' is equal to 'data'
