Meta:
    @group plugin-web-ui
    @testSetId AAMH-5618

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I click on a link with the URL containing '$URLpart' and the text '$text'
Meta:
    @testCaseId AAMH-3729
Then the page title is 'Hyperion Template'
When I click on a link with the URL containing 'page' and the text 'This is a link'
Then the page title is 'Hyperion Template - Other Page'

Scenario: Step verification When I hover a mouse over a link with the text
Meta:
    @testCaseId AAMH-3730
Then an element with the name '<elementText>' exists
When I hover a mouse over a link with the text '<elementText>'
Then an element with the name '<elementText>' does not exist
Examples:
|elementText            |
|Go to the main page    |

Scenario: Step verification When I click on a link with the CSS selector
Meta:
    @testCaseId AAMH-3731
Then the page title is 'Hyperion Template - Other Page'
When I click on a link with the CSS selector 'a[class^='link']'
Then the page title is 'Hyperion Template'

Scenario: Step verification When I click on a link with the text
Meta:
    @testCaseId AAMH-3732
When I wait until the page has the title 'Hyperion Template'
When I click on a link with the text 'This is a link'
When I wait until the page has the title 'Hyperion Template - Other Page'

Scenario: Step verification When I hover a mouse over a link with the URL
Meta:
    @testCaseId AAMH-3733
When I change context to an element by the xpath '//div[@id="for-link"]'
Then an element with the name '<elementText>' exists
When I hover a mouse over a link with the URL '/'
When I change context to an element by the xpath '//div[@id="for-link"]'
Then an element with the name '<elementText>' does not exist
Examples:
|elementText            |
|Go to the main page    |

Scenario: Step verification When I click on a link with the text in a row containing text '$textPart' in the table
Meta:
    @testCaseId AAMH-3734
Then the page title is 'Hyperion Template - Other Page'
When I change context to a table with the attribute 'id'='exampleTable'
When I click on a link with the text 'Link to main page' in a row containing text 'February' in the table
Then the page title is 'Hyperion Template'

Scenario: Step verification When I click on a link with the URL
Meta:
    @testCaseId AAMH-3735
When I wait until the page has the title 'Hyperion Template'
When I change context to an element by the xpath '//div[@id='button-container']'
When I click on a link with the URL '/html/page.html'
When I wait until the page has the title 'Hyperion Template - Other Page'

Scenario: Step verification When I click on a link with the URL containing
Meta:
    @testCaseId AAMH-3737
When I go to the relative URL '/'
When I wait until the page has the title 'Hyperion Template'
When I change context to an element by the xpath '//div[@id='button-container']'
When I click on a link with the URL containing 'page'
When I wait until the page has the title 'Hyperion Template - Other Page'

Scenario: Step verification that a link with tooltip does not exist
Meta:
    @testCaseId AAMH-3738
Then a link with the URL '/' and tooltip 'wrongToolTip' does not exist
Then a link with the text 'wrongText' and URL '/html/page1.html' and tooltip 'ImageToolTip' does not exist

Scenario: Step verification Then context contains list of link items with the text and link
Meta:
    @testCaseId AAMH-3739
When I change context to an element by the xpath '//div[@id="listId"]'
Then context contains list of link items with the text and link:
|text     |link                |
|PageOther|/html/pageOther.html|
|MainPage |/                   |

Scenario: Step verification Then context contains list of link items with the text
Meta:
    @testCaseId AAMH-3740
When I change context to an element by the xpath '//div[@id="listId"]'
Then context contains list of link items with the text:
|text     |
|PageOther|
|MainPage |

Scenario: Step verification that a link with tooltip exists
Meta:
    @testCaseId AAMH-3741
Then a link with the text '<linkText>' and tooltip '<Tooltip>' exists
Then a link with the URL '<linkURL>' and tooltip '<Tooltip>' exists
Then a link with the text '<linkText>' and URL '<linkURL>' and tooltip '<Tooltip>' exists
Then a link with the tooltip '<Tooltip>' exists
Examples:
|linkText                       |Tooltip         |linkURL   |
|Text for link with tooltip     |LinkToolTip     |/         |

Scenario: Step verification that a 'state' link with tooltip exists
Meta:
    @testCaseId AAMH-3742
Then a [VISIBLE] link with the text '<linkText>' and URL '<linkURL>' and tooltip '<Tooltip>' exists
Then a [VISIBLE] link with the text '<linkText>' and tooltip '<Tooltip>' exists
Then a [VISIBLE] link with the URL '<linkURL>' and tooltip '<Tooltip>' exists
Then a [VISIBLE] link with the tooltip '<Tooltip>' exists
Examples:
|linkText                       |Tooltip         |linkURL   |
|Text for link with tooltip     |LinkToolTip     |/         |

Scenario: Step verification When I click on a link with the text and URL
Meta:
    @testCaseId AAMH-3744
When I go to the relative URL '/'
When I wait until the page has the title 'Hyperion Template'
When I click on a link with the text 'This is a link' and URL '/html/page.html'
When I wait until the page has the title 'Hyperion Template - Other Page'

Scenario: Step verification When I click on an image with the tooltip
Meta:
    @testCaseId AAMH-3745
Then the page title is 'Hyperion Template - Other Page'
When I click on an image with the tooltip 'ImageToolTip'
Then the page title is 'Hyperion Template'

Scenario: Step verification that a link exists
Meta:
    @testCaseId AAMH-3746
When I change context to an element by the xpath '//div[@id='button-container']'
Then a link with the text '<linkText>' and URL containing '<linkPart>' exists
Then a link with the text '<linkText>' and URL '<linkURL>' exists
Then a link with the text '<linkText>' exists
Then a link with the URL '<linkURL>' exists
Then a link with the URL containing '<linkPart>' exists
When I change context to the page
Then a link tag with href '/css/app.css' exists
Examples:
|linkText             |linkURL             |linkPart  |
|This is a link       |/html/page.html     |page      |

Scenario: Step verification that a 'state' link exists
Meta:
    @testCaseId AAMH-3747
Then a [VISIBLE] link with the text '<linkText>' and URL '<linkURL>' exists
Then a [VISIBLE] link with text '<linkText>' and URL containing 'page' exists
Then a [NOT_SELECTED] link with the text '<linkText>' exists
When I change context to an element by the xpath '//div[@id='button-container']'
Then a [VISIBLE] link with the URL '<linkURL>' exists
Examples:
|linkText             |linkURL                      |
|This is a link       |/html/page.html              |

Scenario: Step verification that a link does not exist
Meta:
    @testCaseId AAMH-3748
Then a link with the text 'Learn JSON' does not exist
Then a link with the URL '<wrongURL>' does not exist
Then a link with the text '/html/page.html' and URL '<wrongURL>' does not exist
Examples:
|wrongURL              |
|http://www.wwwww.com  |