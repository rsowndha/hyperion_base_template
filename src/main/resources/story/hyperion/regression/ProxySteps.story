Meta:
    @group plugin-http-proxy
    @proxy
    @testSetId AAMH-5595

Scenario: Verification of step "When I clear the proxy log"
Meta:
    @testCaseId AAMH-3771
Given I am on the main application page
Then the number of calls is EQUAL_TO '2' for url pattern '${mainPageURL}img/imageToDisappear.jpg?.*'
When I clear the proxy log
Then the number of calls is EQUAL_TO '0' for url pattern '${mainPageURL}img/imageToDisappear.jpg?.*'

Scenario: Verification of step "Then the number of calls is $comparisonRule '$number' for url pattern '$urlPattern'"
Meta:
    @testCaseId AAMH-3772
Given I am on the main application page
Then the number of calls is EQUAL_TO '2' for url pattern '${mainPageURL}img/imageToDisappear.jpg?.*'

Scenario: Verification of step "When I mock http requests with parameters: $parameters"
Meta:
    @testCaseId AAMH-3773
Given I am on the main application page
When I mock http requests with parameters: 
|urlPattern     |requestParameterNames  |requestParameterValues|
|/html/page.html|<requestParameterNames>|<requestParameterValues>|
When I click on a link with the text 'Clickable link 2'
Then the text '<text>' exists
Examples:
|requestParameterNames|requestParameterValues|text                                                                |
|name                 |linkCallNumber        |Successfully validated request parameters: [name: linkCallNumber]|
|name                 |unknownName           |Failed to validate request parameters (parameters have different values from expected). Actual request: ${mainPageURL}html/page.html?name=linkCallNumber Expected parameters: [name: unknownName]|
