Meta:
    @group plugin-web-ui
    @testSetId AAMH-5620

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification that a javascript file exists or does not exist
Meta:
    @testCaseId AAMH-3777
Then a javascript file with the name 'app' is included in the source code
Then a javascript with the text 'document.getElementById("demo-for-script").innerHTML = "Hello JavaScript!";' is included in the source code
Then a javascript with the textpart 'Hello JavaScript' is included in the source code
