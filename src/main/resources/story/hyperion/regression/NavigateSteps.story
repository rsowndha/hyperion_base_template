Meta:
    @group plugin-web-ui
    @testSetId AAMH-5615

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Check switching to a new window
Meta:
    @testCaseId AAMH-3752
When I click on an element with the text 'OpenNewWindow'
Then the page title is 'Hyperion Template'
When I switch to a new window
Then the page title contains the text 'Other Page'
When I close the current window

Scenario: Check switching to a new window with the name containing '$windowPartName'
Meta:
    @testCaseId AAMH-3753
When I click on an element with the text 'OpenNewWindow'
Then the text 'My First Heading' does not exist
When I switch to a window with the name containing 'Other Page'
Then the page with the URL containing 'html/page.html' is loaded
When I close the current window

Scenario: Check page refreshing
Meta:
    @testCaseId AAMH-3754
When I go to the relative URL 'html/pageOther.html'
When I refresh the page
Then the page with the URL '${mainPageURL}html/pageOther.html' is loaded
