Meta:
    @group plugin-web-ui
    @testSetId AAMH-5599

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I select the '$previousNext' slide in the carousel
Meta:
    @testCaseId AAMH-3616
When I change context to a [VISIBLE] carousel
When I select a slide with the slide number '1' in a carousel
Then an image with the src '/img/2.jpeg' does not exist
When I select the 'right carousel-control' slide in the carousel
When I wait until an element with the tag 'img' and attribute 'src'='/img/1.jpeg' disappears
When I wait until an element with the tag 'img' and attribute 'src'='/img/2.jpeg' appears
Then an image with the src '/img/2.jpeg' exists

Scenario: Step verification When I select a slide with the slide number
Meta:
    @testCaseId AAMH-3617
When I change context to a carousel with the attribute 'class'='row carousel-row full width'
When I select a slide with the slide number '1' in a carousel
Then an image with the src '/img/3.jpeg' does not exist
When I select a slide with the slide number '3' in a carousel
When I wait until an element with the tag 'img' and attribute 'src'='/img/3.jpeg' appears

Scenario: Step verification that a [$state] carousel exists
Meta:
    @testCaseId AAMH-3618
Then a [VISIBLE] carousel exists
When I change context to a [VISIBLE] carousel with the attribute 'class'='row carousel-row full width'
Then there are '4' slides in a carousel

Scenario: Step verification that a carousel exists
Meta:
    @testCaseId AAMH-3619
Then a carousel exists
When I change context to a carousel
Then there are '4' slides in a carousel
