Meta:
    @group plugin-web-ui
    @testSetId AAMH-5607

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification that a drop down exists or does not exist
Meta:
    @testCaseId AAMH-3638
Then a drop down with the name 'cats' does not exist
Then a drop down with the name '<dropDownNameCars>' exists
Then a [ENABLED] drop down with the name '<dropDownNameCars>' exists
Then a drop down with the name '<dropDownNameCars>' and text 'Fiat' exists
Examples:
|dropDownNameCars     |
|cars                 |

Scenario: Step verification When I select 'text' from a drop down with the name
!-- Skipped due to jbrowser bug https://www.jjconsumer.com/jira/browse/DTE-2910
Meta:
    @skip
Then a drop down with the name '<dropDownNameCars>' contains the items:
|item          |state |
|Volvo         |false |
|Saab          |false |
|Fiat          |true  |
|Audi          |false |
When I select 'Volvo' from a drop down with the name '<dropDownNameCars>'
Then a drop down with the name '<dropDownNameCars>' contains the items:
|item          |state |
|Volvo         |true  |
|Saab          |false |
|Fiat          |false |
|Audi          |false |
Examples:
|dropDownNameCars     |
|cars                 |

Scenario: Step verification When I add 'text' to selection in a drop down with the name
!-- Skipped due to jbrowser bug https://www.jjconsumer.com/jira/browse/DTE-2910
Meta:
    @skip
Then a drop down with the name '<dropDownNameCars>' contains the items:
|item          |state |
|Volvo         |true  |
|Saab          |false |
|Fiat          |false |
|Audi          |false |
When I add 'Audi' to selection in a drop down with the name '<dropDownNameCars>'
Then a drop down with the name '<dropDownNameCars>' contains the items:
|item          |state |
|Volvo         |true  |
|Saab          |false |
|Fiat          |false |
|Audi          |true  |
Examples:
|dropDownNameCars     |
|cars                 |