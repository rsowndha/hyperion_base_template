Meta:
    @group plugin-pdf
    @testSetId AAMH-32536

Scenario: Step verification When I save the text of the PDF document from the response to the $scope variable '$variableName'
Meta:
    @testCaseId AAMH-32537
When I send HTTP GET to the relative URL '/test.pdf'
When I save the text of the PDF document from the response to the SCENARIO variable 'textFromResponse'
Then '#{eval(`${textFromResponse}`.replaceAll("\s", ""))}' is equal to 'test'

Scenario: Step verification Then the PDF document from response contains links: $links
Meta:
    @testCaseId AAMH-32538
When I send HTTP GET to the relative URL '/test-links.pdf'
Then the PDF document from response contains links:
|link                                                       |
|https://predev-contenthub.apps.dnadev.jnj/node/492         |
|https://predev-contenthub.apps.dnadev.jnj/node/492         |
|https://predev-contenthub.apps.dnadev.jnj/user/1           |
|https://predev-contenthub.apps.dnadev.jnj/taxonomy/term/91 |
|https://predev-contenthub.apps.dnadev.jnj/taxonomy/term/162|