Meta:
    @group plugin-rest-api
    @testSetId AAMH-5613

Scenario: Step verification Given request body: content
Meta:
    @testCaseId AAMH-3856
Given request body: {"text":"responseText","number":1}
When I send HTTP POST to the relative URL '/api/get-back-request-body'
Then the response body is equal to '{"text":"responseText","number":1}'

Scenario: Step verification When I create a request using template 'fileName' with parameters: parameters
Meta:
    @testCaseId AAMH-3597
When I create a request using template '/story/request/requestTemplate.ftl' with parameters: 
|id|text|
|1|responseText|
When I send HTTP POST to the relative URL '/api/get-back-request-body'
Then the response body is equal to '{"text":"responseText","number":1}'

Scenario: Step verification When I issue a HTTP httpMethod request for a resource with the URL 'url'
Meta:
    @testCaseId AAMH-3598
Given request body: #{loadResource(/story/request/requestBody.txt)}
When I issue a HTTP POST request for a resource with the URL '${mainPageURL}/api/get-back-request-body'
Then the response body is equal to '{"text":"responseText","number":1}'

Scenario: Step verification When I send HTTP httpMethod to the relative URL 'relativeURL'
Meta:
    @testCaseId AAMH-3599
Given request body: #{loadResource(/story/request/requestBody.txt)}
When I send HTTP POST to the relative URL '/api/get-back-request-body'
Then the response body is equal to '{"text":"responseText","number":1}'

Scenario: Step verification When I send HTTP httpMethod to the relative URL 'relativeURL' with content: 'content'
Meta:
    @testCaseId AAMH-3600
When I send HTTP POST to the relative URL '/api/get-back-request-body' with content: '{"text":"responseText","number":1}'
Then the response body is equal to '{"text":"responseText","number":1}'

Scenario: Step verification When I set request headers: headers
Meta:
    @testCaseId AAMH-3601
When I set request headers:
|name   |value|
|header1|headerValue|
When I send HTTP GET to the relative URL '/api/get-response'
When I save response header 'request-header1' value to SCENARIO variable 'actualHeader'
Then '${actualHeader}' is EQUAL_TO 'headerValue'

Scenario: Step verification Then the response does not contain body
Meta:
    @testCaseId AAMH-3602
When I send HTTP DELETE to the relative URL '/api/get-back-request-body'
Then the response does not contain body
