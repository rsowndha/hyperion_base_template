Meta:
    @group plugin-rest-api
    @testSetId AAMH-5628

Lifecycle:
Before:
Scope: STORY
When I send HTTP GET to the relative URL '/api/get-response'

Scenario: Step verification Then a JSON element by the JSON path 'jsonPath' is equal to 'expectedData'
Meta:
    @testCaseId AAMH-3717
Then a JSON element by the JSON path '$.text' is equal to 'responseText'

Scenario: Step verification Then a JSON element by the JSON path 'jsonPath' exists
Meta:
    @testCaseId AAMH-3718
Then a JSON element by the JSON path '$.text' exists

Scenario: Step verification Then the number of JSON elements by the JSON path 'jsonPath' is comparisonRule elementsNumber
Meta:
    @testCaseId AAMH-3719
Then the number of JSON elements by the JSON path '$.text' is equal to 1

Scenario: Step verification When I save a JSON element from response by JSON path 'jsonPath' to scope variable 'variableName'
Meta:
    @testCaseId AAMH-3720
When I save a JSON element from response by JSON path '$.number' to SCENARIO variable 'responseNumber'
Then '${responseNumber}' is equal to '1'

Scenario: Step verification When I save a JSON element from 'json' by JSON path 'jsonPath' to scope variable 'variableName'
Meta:
    @testCaseId AAMH-3721
When I save a JSON element from '{"number":1}' by JSON path '$.number' to SCENARIO variable 'jsonNumber'
Then '${jsonNumber}' is equal to '1'

Scenario: Step verification When I save the biggest JSON element from 'json' by JSON path 'jsonPath' to scope variable 'variableName'
Meta:
    @testCaseId AAMH-3722
When I save a JSON element from '{"number": 3}' by JSON path '$.number' to SCENARIO variable 'number'
When I save the biggest JSON element from '{"number":[0, 1, 2, 3]}' by JSON path '$.number' to SCENARIO variable 'theBiggestNumber'
Then '${number}' is equal to '${theBiggestNumber}'

Scenario: Step verification When I set the number of elements found by the JSON path 'jsonPath' to the scope variable 'variableName'
Meta:
    @testCaseId AAMH-23743
When I set the number of elements found by the JSON path '$.number' to the SCENARIO variable 'count'
Then the number of JSON elements by the JSON path '$.number' is equal to ${count}

Scenario: Step verification Then the date 'date' is comparisonRule current for seconds seconds
Meta:
    @testCaseId AAMH-3723
Then the date '2017-01-04T14:08:56' is greater than current for 5000 seconds

Scenario: Step verification Then the date 'date' in the format 'format' is comparisonRule current for seconds seconds
Meta:
    @testCaseId AAMH-3724
Then the date '2017-01-04T14:08:56.235-0700' in the format 'yyyy-MM-dd'T'HH:mm:ss.SSSZ' is greater than current for 5000 seconds

Scenario: Step verification Then the date 'date1' is comparisonRule the date 'date2'
Meta:
    @testCaseId AAMH-3725
Then the date '2017-01-04T14:08:56' is greater than the date '2016-01-04T14:08:56'

Scenario: Step verification When I find $comparisonRule '$elementsNumber' JSON elements by '$jsonPath' and for each element do$stepsToExecute
Meta:
    @testCaseId AAMH-27138
When I find >= '1' JSON elements by '$.text' and for each element do
|step|
|Then a JSON element by the JSON path '$' is equal to 'responseText'|

Scenario: Step verification When I wait for presence of the element by JSON path '$jsonPath' in HTTP GET response from '$resourceUrl' for '$duration' duration
Meta:
    @testCaseId AAMH-4978
When I send HTTP DELETE to the relative URL '/api/call-number?name=belatedCallNumber'
Given request body: {"name":"belatedCallNumber"}
When I set request headers:
|name        |value           |
|Accept      |application/json|
|Content-Type|application/json|
When I send HTTP POST to the relative URL '/api/call-number'
When I wait for presence of the element by JSON path '$.belatedCallNumber' in HTTP GET response from '${mainPageURL}/api/call-number?name=belatedCallNumber' for 'PT1S' duration