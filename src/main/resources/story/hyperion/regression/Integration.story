Meta:
    @group plugin-web-ui + plugin-rest-api
    @testSetId AAMH-5594

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Retrieve JSON data
Meta:
    @testCaseId AAMH-3711
When I issue a HTTP GET request for a resource with the URL '${mainPageURL}/api/get-response'

Scenario: Step verification "When I set the text found in search context to the '$scope' variable '$variableName'" and "Then a JSON element by the JSON path '$jsonPath' is equal to '$expectedJson'"
Meta:
    @testCaseId AAMH-3712
When I change context to an element with the attribute 'name'='responseText'
When I set the text found in search context to the 'SCENARIO' variable 'elementText'
Then a JSON element by the JSON path 'text' is equal to '${elementText}'

Scenario: Step verification "When I set '$attributeName' attribute value of the context element to the '$scope' variable '$variableName'" and "Then a JSON element by the JSON path '$jsonPath' is equal to '$expectedJson'"
Meta:
    @testCaseId AAMH-3713
When I change context to an element with the attribute 'name'='responseValue'
When I set 'value' attribute value of the context element to the 'SCENARIO' variable 'elementAttribute'
Then a JSON element by the JSON path 'text' is equal to '${elementAttribute}'

Scenario: Retrieve XML data
Meta:
    @testCaseId AAMH-3714
When I set request headers:
|name|value|
|Accept|application/xml|
When I send HTTP GET to the relative URL '/api/get-response'

Scenario: Step verification "When I set the text found in search context to the '$scope' variable '$variableName'" and "Then the xml document contains the data '$expectedData' by the xpath '$xpath'"
Meta:
    @testCaseId AAMH-3715
When I change context to an element by the xpath '//*[@name='responseText']'
When I set the text found in search context to the 'SCENARIO' variable 'elementText'
Then the xml document contains the data '${elementText}' by the xpath '/response/text/text()'

Scenario: Step verification "When I set '$attributeName' attribute value of the context element to the '$scope' variable '$variableName'" and "Then the xml document contains the data '$expectedData' by the xpath '$xpath'"
Meta:
    @testCaseId AAMH-3716
When I change context to an element by the xpath '//*[@name='responseText']'
When I set 'name' attribute value of the context element to the 'SCENARIO' variable 'elementAttribute'
Then the xml document contains the data '${elementAttribute}' by the xpath '/response/text/text()'
