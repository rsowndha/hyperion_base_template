Meta:
    @group plugin-web-ui
    @testSetId AAMH-5590

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario:  Verification When I get the value from the URL and set it to the '$type' variable '$variable' step
Meta:
    @testCaseId AAMH-3786
When I go to the relative URL '/html/page.html'
When I get the value from the URL and set it to the 'STORY' variable 'URL'
When I go to the relative URL '/'
Then the page title is 'Hyperion Template'
When I go to the relative URL '/html/${URL}'
Then the page title is 'Hyperion Template - Other Page'

Scenario:  Verification When I get the value from the URL and set.. already set variable
Meta:
    @testCaseId AAMH-3787
When I go to the relative URL '/'
Then the page title is 'Hyperion Template'
When I go to the relative URL '/html/${URL}'
Then the page title is 'Hyperion Template - Other Page'

Scenario: Verification When I navigate back
Meta:
    @testCaseId AAMH-22584
When I go to the relative URL '/html/page.html'
When I navigate back
Then the page with the URL '${mainPageURL}' is loaded
