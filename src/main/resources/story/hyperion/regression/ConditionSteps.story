Meta:
    @group hyperion
    @testSetId AAMH-28645

Scenario: Step verification When the condition '$condition' is true I do$stepsToExecute
Meta:
    @testCaseId AAMH-28624
When the condition '<condition>' is true I do
|step                                        |
|Then 'to be validated' is equal to '<value>'|
Examples:
|condition |value                    |
|true      |to be validated          |
|false     |should be never validated|