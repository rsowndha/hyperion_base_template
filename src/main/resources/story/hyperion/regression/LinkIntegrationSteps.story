Meta:
    @group plugin-web-integration
    @testSetId AAMH-5609

Scenario: Step verification Then a link with the name '$linkName' is clickable
Meta:
    @testCaseId AAMH-3727
Given I am on the main application page
When I send HTTP DELETE to the relative URL '/api/call-number?name=linkCallNumber'
Then a link with the name 'Clickable link 2' is clickable
When I send HTTP GET to the relative URL '/api/call-number?name=linkCallNumber'
Then the response body is equal to '{"linkCallNumber":1}'

Scenario: Step verification Then a link with the name '$linkName' and URL '$URL' is clickable
Meta:
    @testCaseId AAMH-3728
When I send HTTP DELETE to the relative URL '/api/call-number?name=linkCallNumber'
Then a link with the name 'Clickable link 2' and URL '/html/page.html?name=linkCallNumber' is clickable
When I send HTTP GET to the relative URL '/api/call-number?name=linkCallNumber'
Then the response body is equal to '{"linkCallNumber":1}'