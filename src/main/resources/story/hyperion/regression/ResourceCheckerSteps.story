Meta:
    @group plugin-web-integration
    @testSetId AAMH-5614

Scenario: Step verification Then I check all links
Meta:
    @testCaseId AAMH-3774
Given I am on the main application page
When I change context to an element with the name 'valid-resources'
Then I check all links

Scenario: Step verification Then all resources of types are valid: $types
Meta:
    @testCaseId AAMH-3775
Given I am on the main application page
When I change context to an element with the name 'valid-resources'
Then all resources of types are valid: LINK, IMAGE, FRAME
