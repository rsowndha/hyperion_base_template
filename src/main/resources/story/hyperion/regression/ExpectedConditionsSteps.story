Meta:
    @group plugin-web-ui
    @testSetId AAMH-5612

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification Then the page title contains the text
Meta:
    @testCaseId AAMH-3655
Then the page title contains the text 'Hyperion'
Then the page title is 'Hyperion Template'

!-- Skipped due to jbrowser bug https://github.com/MachinePublishers/jBrowserDriver/issues/147
Scenario: Step verification Then an alert is present or not present
Meta:
    @skip
Then an alert is not present
When I click on a button with the name 'Click Me!'
When I wait until an alert appears
Then an alert is present