Meta:
    @group plugin-rest-api
    @testSetId AAMH-5596
    
Scenario: Step verification Then the xml document contains the data 'expectedData' by the xpath 'path'
Meta:
    @testCaseId AAMH-3802
When I set request headers:
|name|value|
|Accept|application/xml|
When I send HTTP GET to the relative URL '/api/get-response'
Then the xml document contains the data 'responseText' by the xpath '/response/text/text()'

Scenario: Step verification Then the xml document contains an element by the xpath 'xpath'
Meta:
    @testCaseId AAMH-3803
When I set request headers:
|name|value|
|Accept|application/xml|
When I send HTTP GET to the relative URL '/api/get-response'
Then the xml document contains an element by the xpath '/response/text'

Scenario: Step verification Then the xml document is equal to 'expectedXml'
Meta:
    @testCaseId AAMH-3804
When I set request headers:
|name|value|
|Accept|application/xml|
When I send HTTP GET to the relative URL '/api/get-response'
Then the xml document is equal to '#{loadResource(/story/xml/data.xml)}'

Scenario: Step verification Then the xml document validated by xsd 'pathToXsd' is valid
Meta:
    @testCaseId AAMH-3805
When I set request headers:
|name|value|
|Accept|application/xml|
When I send HTTP GET to the relative URL '/api/get-response'
Then the xml document validated by xsd '/story/xml/validate.xsd' is valid

Scenario: Step verification Then the xml document transformed with 'xslFilePath' is equal to 'expectedXml'
Meta:
    @testCaseId AAMH-3806
Then the xml document transformed with '/story/xml/transform.xslt' is equal to '#{loadResource(/story/xml/data.html)}'
