Meta:
    @group plugin-web-ui
    @testSetId AAMH-5600

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification Then the table contains '$rowCount' rows
Meta:
    @testCaseId AAMH-3796
When I change context to an element by the xpath '//h4[contains(text(),"3 Rows and 3 Columns")]/following-sibling::table'
Then the table contains '3' rows

Scenario: Step verification that the text exists or does not exist
Meta:
    @testCaseId AAMH-3797
Then the text '500' exists
Then the text '5001' does not exist

Scenario: Step verification that a frame or scrollbar exists
Meta:
    @testCaseId AAMH-3798
Then a frame with the attribute 'name'='iframe' exists
Then a [VISIBLE] frame with the attribute 'name'='iframe' exists

Scenario: Go to the relative URL
Meta:
    @testCaseId AAMH-3799
When I go to the relative URL '/html/pageOther.html'

Scenario: Step verification Then there are breadcrumbs with the text and url and separator
Meta:
    @testCaseId AAMH-3800
Then there are breadcrumbs with the text and url and separator:
|text            |url            |separator  |
|MainPage        |/              |>          |

Scenario: Step verification Then the text matches
Meta:
    @testCaseId AAMH-3801
When I change context to an element by the xpath '//button[@name='button-show-json']'
Then the text matches '^Show'
