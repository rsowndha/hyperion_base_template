Meta:
    @group plugin-web-ui
    @testSetId AAMH-5603

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Add text and clear a field
Meta:
    @testCaseId AAMH-3691
Then a field with the name 'message' exists
Then a field with the name 'message' and text 'The cat was playing in the garden.' exists
Then a field with the name 'message' and text containing 'in the garden' exists
When I add 'The weather was good.' to a field with the name 'message'
When I click on a button with the name 'addElement'
Then an element with the name 'inputText' and text 'The cat was playing in the garden.The weather was good.' exists
When I clear a field with the name 'message'
When I click on a button with the name 'addElement'
Then an element with the name 'inputText' and text '' exists
Then a field with the name 'notExistingFieldName' does not exist

Scenario: Find fields with statement
Meta:
    @testCaseId AAMH-3692
Then a field with the name 'notExistingFieldName' and text 'The cat was playing in the garden.' does not exist
Then a [enabled] field with the name 'firstname' exists
When I enter 'First Name' in a field with the name 'firstname'
Then a [enabled] field with the name 'firstname' and text 'First Name' exists

Scenario: Find fields with placeholder
Meta:
    @testCaseId AAMH-3693
Then a field with the name 'placeholderArea' and placeholder text 'This is placeholder text.' exists
Then a [disabled] field with the name 'placeholderArea' and placeholder text 'This is placeholder text.' exists
Then a field with the name 'notExistingFieldName' placeholder text 'This is placeholder text.' does not exist
