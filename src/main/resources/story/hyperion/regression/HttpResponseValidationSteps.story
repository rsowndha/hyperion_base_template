Meta:
    @group plugin-rest-api
    @testSetId AAMH-5602

Lifecycle:
Before:
Scope: STORY
When I send HTTP GET to the relative URL '/api/get-response'

Scenario: Step verification "Then the response body is equal to '$content'"
Meta:
    @testCaseId AAMH-3694
Then the response body is equal to '{"text":"responseText","number":1}'

Scenario: Step verification "Then the response body contains '$content'"
Meta:
    @testCaseId AAMH-3695
Then the response body contains '{"text":"responseText","number":1}'

Scenario: Step verification "Then the response body does not contain '$content'"
Meta:
    @testCaseId AAMH-3696
Then the response body does not contain '{"id":1}'

Scenario: Step verification "Then the response code is equal to '$responseCode'"
Meta:
    @testCaseId AAMH-3697
Then the response code is equal to '200'

Scenario: Step verification "Then the response time should be less than '$responseTimeThresholdMs' milliseconds"
Meta:
    @testCaseId AAMH-3698
Then the response time should be less than '5000' milliseconds

Scenario: Step verification "Then response header '$httpHeaderName' contains attribute: $attributes"
Meta:
    @testCaseId AAMH-3699
Then response header 'Content-Type' contains attribute: 
|attribute|
|application/json|

Scenario: Step verification "Then the value of the response header '$httpHeaderName' $validationRule '$value'"
Meta:
    @testCaseId AAMH-20722
Then the value of the response header 'Content-Type' is equal to 'application/json;charset=UTF-8'

Scenario: Step verification "When I save response header '$httpHeaderName' value to $scope variable '$variableName'"
Meta:
    @testCaseId AAMH-3700
When I save response header 'Transfer-Encoding' value to SCENARIO variable 'headerValue'
Then response header 'Transfer-Encoding' contains attribute: 
|attribute|
|${headerValue}|

Scenario: Step verification "Then the number of the response headers with the name '$headerName' is $comparisonRule $value"
Meta:
    @testCaseId AAMH-25293
Then the number of the response headers with the name 'Transfer-Encoding' is = 1
