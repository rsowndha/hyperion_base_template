Meta:
    @group plugin-web-integration
    @testSetId AAMH-5619

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I perform a HTTP GET request for a resource with the URL '$url' and save response body to the file '$fileName'
Meta:
    @testCaseId AAMH-3776
When I perform a HTTP GET request for a resource with the URL '${mainPageURL}/api/get-response' and save response body to the file 'story/response/responseFile.txt'
When I issue a HTTP GET request for a resource with the URL '${mainPageURL}/api/get-response'
Then the response body is equal to '#{loadResource(/story/response/responseFile.txt)}'
