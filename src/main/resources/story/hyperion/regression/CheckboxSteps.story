Meta:
    @group plugin-web-ui
    @testSetId AAMH-5592

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I check all the checkboxes
Meta:
    @testCaseId AAMH-3620
Then a [NOT_SELECTED] checkbox with the name '<name1>' exists
Then a [NOT_SELECTED] checkbox with the attribute '<attributeValueName>'='<text2>' exists
When I check all the checkboxes
Then a [SELECTED] checkbox with the name '<name1>' exists
Then a [SELECTED] checkbox with the attribute '<attributeValueName>'='<text2>' exists
Examples:
|text2            |attributeValueName   |name1         |
|vehicle2         |name                 |I have a bike |

Scenario: Step verification When I uncheck a checkbox by the xpath
Meta:
    @testCaseId AAMH-3621
Then a [SELECTED] checkbox with the name '<name1>' exists
When I uncheck a checkbox by the xpath '//input[@type='checkbox'][@name='vehicle1']'
Then a [NOT_SELECTED] checkbox with the name '<name1>' exists
Examples:
|name1         |
|I have a bike |

Scenario: Step verification When I uncheck a checkbox with the attribute
Meta:
    @testCaseId AAMH-3622
Then a [SELECTED] checkbox with the attribute '<attributeValueName>'='<text2>' exists
When I uncheck a checkbox with the attribute '<attributeValueName>'='<text2>'
Then a [NOT_SELECTED] checkbox with the attribute '<attributeValueName>'='<text2>' exists
Examples:
|text2            |attributeValueName   |
|vehicle2         |name                 |

Scenario: Step verification When I check a checkbox by the xpath
Meta:
    @testCaseId AAMH-3623
Then a [NOT_SELECTED] checkbox with the name '<name2>' exists
When I check a checkbox by the xpath '//input[@type='checkbox'][@name='vehicle2']'
Then a [SELECTED] checkbox with the name '<name2>' exists
Examples:
|name2        |
|I have a car |

Scenario: Step verification that I uncheck a checkbox with the name
Meta:
    @testCaseId AAMH-3624
Then a [SELECTED] checkbox with the name '<name2>' exists
When I uncheck a checkbox with the name '<name2>'
Then a [NOT_SELECTED] checkbox with the name '<name2>' exists
Examples:
|name2        |
|I have a car |

Scenario: Step verification that I check a checkbox with the name
Meta:
    @testCaseId AAMH-3625
Then a [NOT_SELECTED] checkbox with the name '<name1>' exists
When I check a checkbox with the name '<name1>'
Then a [SELECTED] checkbox with the name '<name1>' exists
Examples:
|name1         |
|I have a bike |

Scenario: Step verification that I check a checkbox with the attribute
Meta:
    @testCaseId AAMH-3626
Then a [NOT_SELECTED] checkbox with the attribute '<attributeValueName>'='<text2>' exists
When I check a checkbox with the attribute '<attributeValueName>'='<text2>'
Then a [SELECTED] checkbox with the attribute '<attributeValueName>'='<text2>' exists
Examples:
|text2            |attributeValueName   |
|vehicle2         |name                 |

Scenario: Step verification that a checkbox exists or does not exist
Meta:
    @testCaseId AAMH-3627
Then a checkbox with the attribute 'name'='vehicle2' exists
Then a checkbox with the name 'I have a bike' exists
Then a checkbox with the name 'I have a cat' does not exist

Scenario: Step verification that I check a checkbox
Meta:
    @testCaseId AAMH-3628
When I change context to an element by the xpath '//div[@id="for-bike"]'
When I check a checkbox
Then a [SELECTED] checkbox with the name 'I have a bike' exists

Scenario: Then a [$state] checkbox with the name '$checkboxName' exists
Meta:
    @testCaseId AAMH-3629
When I uncheck a checkbox with the attribute 'id'='bike'
Then a [NOT_SELECTED] checkbox with the name '<name1>' exists
When I change context to an element by the xpath '//div[@id="for-bike"]'
When I check a checkbox
Then a [SELECTED] checkbox with the name '<name1>' exists
Examples:
|name1         |
|I have a bike |