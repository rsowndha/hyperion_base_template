Meta:
    @group plugin-web-ui
    @testSetId AAMH-5624

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I hover a mouse over a button with the name
Meta:
    @testCaseId AAMH-3612
Then an element with the name '<elementText>' exists
When I hover a mouse over a button with the name 'button-without-alert'
Then an element with the name '<elementText>' does not exist
Examples:
|elementText            |
|ButtonForHover         |

Scenario: Step verification When I click on a button with the name
!-- Skipped due to jbrowser bug https://github.com/MachinePublishers/jBrowserDriver/issues/147
Meta:
    @skip 
Then a button with the name '<buttonNameClick>' exists
When I click on a button with the name '<buttonNameClick>'
When I confirm an action in an alert with the message 'Hello World!'
Examples:
|buttonNameClick     |
|Click Me!           |

Scenario: Step verification that a button exists or does not exist
Meta:
    @testCaseId AAMH-3613
Then a button with the name 'wrongName' does not exist
Then a button with the tooltip 'MyToolTip' and image with the src '/img/hyperion.jpg' exists
Then a [ENABLED] button with the name 'Click Me!' exists

Scenario: Step verification When I click on a button with the image
!-- Skipped due to jbrowser bug https://github.com/MachinePublishers/jBrowserDriver/issues/147
Meta:
    @skip
Then a button with image with the src '<srcImg>' exists
When I click on a button with the image src '<srcImg>'
When I confirm an action in an alert with the message 'Hello World 2!'
Examples:
|srcImg             |
|/img/hyperion.jpg  |

Scenario: Step verification When I select a radio button with the name
Meta:
    @testCaseId AAMH-3614
Then a radio button with the name '<radioOptionOther>' exists
Then a [NOT_SELECTED] radio button with the name '<radioOptionOther>' exists
When I select a radio button with the name '<radioOptionOther>'
Then a [SELECTED] radio button with the name '<radioOptionOther>' exists
Then a [NOT_SELECTED] radio button with the name 'Male' exists
Examples:
|radioOptionOther  |
|Other             |

Scenario: Step verification Then a [$state] radio button with the name '$radioOption' exists
Meta:
    @testCaseId AAMH-3615
When I select a radio button with the name '<radioOptionFemale>'
Then a [SELECTED] radio button with the name '<radioOptionFemale>' exists
Then a [NOT_SELECTED] radio button with the name 'Male' exists
Examples:
|radioOptionFemale |
|Female            |