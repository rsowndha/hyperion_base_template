Meta:
	@givenStory1

Lifecycle:
Examples:
|givenStory1Key   |overridenKey|
|givenStory1Value1|givenStory1 |

Scenario: Given story 1 scenario
Meta:
	@given1
	@preconditionId AAMH-20344

GivenStories: /story/hyperion/regression/precondition/GivenStory2.story

Then '<overridenKey>' is EQUAL_TO 'givenStory1'
Then '<rootKey>' is EQUAL_TO 'rootValue'
Then '<scenarioKey>' is EQUAL_TO 'givenStoryKey'
Examples:
|count|scenarioKey  |
|2    |givenStoryKey|
|3    |givenStoryKey|
