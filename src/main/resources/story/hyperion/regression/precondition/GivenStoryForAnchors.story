Scenario: Scenario-level GivenStories for meta filter anchors
Meta:
    @id given1
    @preconditionId AAMH-9298
Then '<expected>' is equal to '<actual>'

Scenario: Scenario-level GivenStories for examples row anchors
Meta:
    @id given2
    @preconditionId AAMH-9299
Then '<second_expected>' is equal to '<second_actual>'
