Lifecycle:
Examples:
|actual      |expected    |
|some data   |some data   |
|another data|another data|

Scenario: Input data is passed from Lifecycle examples to this scenario
Meta:
    @preconditionId AAMH-9274; AAMH-10098
Then '<expected>' is equal to '<actual>'
Then '<internalExpected>' is equal to '<internalActual>'
Examples:
|internalExpected|internalActual|
|internal        |internal      |
|internal2       |internal2     |
