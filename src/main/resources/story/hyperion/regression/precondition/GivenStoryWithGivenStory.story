GivenStories: story/hyperion/regression/precondition/GivenStoryUsingExamplesFromBase.story

Scenario: Input data is passed from base story to the normal scenario in the first-level GivenStory
Meta:
    @preconditionId AAMH-9303
Then '<expected>' is equal to '<actual>'
