Scenario: Input data is passed from base story to the normal scenario
Meta:
    @preconditionId AAMH-9300
Then '<expected>' is equal to '<actual>'

Scenario: Input data is passed from base story to the scenario with Examples
Meta:
    @preconditionId AAMH-9301
Then '<expected>' is equal to '<actual>'
Then '<internalExpected>' is equal to '<internalActual>'
Examples:
|internalExpected|internalActual|
|internal        |internal      |
|internal2       |internal2     |

Scenario: Input data passed from base story does not overrride Examples from the scenario
Meta:
    @preconditionId AAMH-9302
Then '<expected>' is not equal to '<actual>'
Examples:
|actual |
|internal|
