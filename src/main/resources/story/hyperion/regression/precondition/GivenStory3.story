Meta:
	@givenStory3

Lifecycle:
Examples:
|overridenKey|
|givenStory3 |

Scenario: Given story 3 scenario
Meta:
	@given3
	@preconditionId AAMH-19650

Then '<overridenKey>' is EQUAL_TO 'givenStory3'
Then '<rootKey>' is EQUAL_TO 'rootValue'
Then '<scenarioRootKey>' is EQUAL_TO 'scenarioRootValue3'
Examples:
|scenarioRootKey   |
|scenarioRootValue3|