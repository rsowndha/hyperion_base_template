Scenario: Simple scenario to be executed after simple given story (after failed as well)
Meta:
    @complexGiven
    @preconditionId AAMH-20370
GivenStories: /story/hyperion/regression/precondition/SimpleGivenStory.story
Then 'data' is equal to 'data'
