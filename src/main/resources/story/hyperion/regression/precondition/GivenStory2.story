Meta:
    @givenStory2

Lifecycle:
Examples:
|givenStory2Key   |overridenKey|
|givenStory2Value1|givenStory2 |

Scenario: Given story 2 scenario
Meta:
	@given2
	@preconditionId AAMH-19651

Then '<count>' is EQUAL_TO '1'
Then '<overridenKey>' is EQUAL_TO 'givenStory2'
Then '<givenStory1Key>' is EQUAL_TO 'givenStory1Value1'
Then '<givenStory2Key>' is EQUAL_TO 'givenStory2Value1'
Then '<rootKey>' is EQUAL_TO 'rootValue'
Then '<scenarioKey>' is EQUAL_TO 'givenStoryKey'
Examples:
|count|
|1    |