Meta:
    @group plugin-db
    @testSetId AAMH-28647

Scenario: Step verification "When I execute SQL query '$sqlQuery' and save the result to the $scope variable '$variableName'"
Meta:
    @testCaseId AAMH-22426
When I execute SQL query 'SELECT name FROM USERS WHERE id=1' and save the result to the SCENARIO variable 'dbOutput'
Then '${dbOutput[0].NAME}' is = 'nick'

Scenario: Step verification "Then data from '$sourceSqlQuery' is equal to data from '$targetSqlQuery' matching rows using keys:$keys"
Meta:
    @testCaseId AAMH-32943
Then data from 'SELECT * FROM USERS ORDER BY ID' is equal to data from 'SELECT * FROM USERS ORDER BY NAME' matching rows using keys:ID,NAME

Scenario: Step verification "Then '$data' matching rows using '$keys' is equal to data from:$table"
Meta:
    @testCaseId AAMH-32944
When I execute SQL query 'SELECT * FROM USERS' and save the result to the SCENARIO variable 'dbOutput'
Then '${dbOutput}' matching rows using '' is equal to data from:
|ID |NAME   |
|1  |nick   |
|2  |george |
