Meta:
    @group plugin-web-ui
    @testSetId AAMH-5626

Scenario: Step verification Then the page with the URL '$URL' is loaded
Meta:
    @testCaseId AAMH-3755
Given I am on a page with the URL '${mainPageURL}'
Then the page with the URL '${mainPageURL}' is loaded

Scenario: Go to the relative URL
Meta:
    @testCaseId AAMH-3756
When I go to the relative URL '/html/page.html'

Scenario: Step verification Then the page has the relative URL
Meta:
    @testCaseId AAMH-3757
Then the page has the relative URL '/html/page.html'

Scenario: Then the page with the URL containing '$URLpart' is loaded
Meta:
    @testCaseId AAMH-3758
Then the page with the URL containing 'page' is loaded

Scenario: Step verification When I navigate back
Meta:
    @testCaseId AAMH-32945
Given I am on a page with the URL '${mainPageURL}'
When I click on an element by the xpath '//ul[@class='nav navbar-nav']/li/a[text()='Page Other']'
When I navigate back
When I wait until the page has the title 'Hyperion Template'
Then the page title is 'Hyperion Template'

Scenario: Step verification When I navigate forward
Meta:
    @testCaseId AAMH-32946
Given I am on a page with the URL '${mainPageURL}'
When I click on an element by the xpath '//ul[@class='nav navbar-nav']/li/a[text()='Page Other']'
When I navigate back
When I navigate forward
When I wait until the page has the title 'Hyperion Template - Other Page'
Then the page title is 'Hyperion Template - Other Page'
