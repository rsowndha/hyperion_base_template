Meta:
    @group hyperion
    @testSetId AAMH-28640

Scenario: Verification When I initialize the $scope variable '$variableName' with value '$variableValue' step
Meta:
    @testCaseId AAMH-28620
When I initialize the STORY variable 'STORY_VAR' with value 'storyValue'
When I initialize the NEXT_BATCHES variable 'NEXT_BATCHES_VAR' with value 'nextBatchesValue'

Scenario: Verification Then '$variable1' is $comparisonRule '$variable2'
Meta:
    @testCaseId AAMH-28621
Then '${STORY_VAR}' is EQUAL_TO 'storyValue'

Scenario: Verification NEXT_BATCHES variable is not available in first batch
Meta:
    @testCaseId AAMH-28622
Then '${NEXT_BATCHES_VAR}' is NOT_EQUAL_TO 'nextBatchesValue'

Scenario: Verification When I initialize the $scope variable '$variableName' with values:$examplesTable step
Meta:
    @testCaseId AAMH-32648
When I initialize the SCENARIO variable 'SCENARIO_VAR' with values:
|key  |
|value|
Then '${SCENARIO_VAR[0].key}' is = 'value'
