Meta:
    @group hyperion-expressions
    @testSetId AAMH-5601

Scenario: Check Hyperion expression #{formatDate}: remove TZD
Meta:
    @testCaseId AAMH-3656
Then '#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ss.SSS)}' is equal to '2017-01-13T09:00:42.862'
Then '#{formatDate(2017-01-13T09:00:42.862-05:00, yyyy-MM-dd'T'HH:mm:ss.SSS)}' is equal to '2017-01-13T09:00:42.862'

Scenario: Check Hyperion expression #{formatDate}: remove ms
Meta:
    @testCaseId AAMH-3657
Then '<Expression>' is equal to '<Result>'
Examples:
|Expression                                                             |Result                    |
|#{formatDate(2017-01-13T09:00:42.862, yyyy-MM-dd'T'HH:mm:ss)}          |2017-01-13T09:00:42       |
|#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ss)}         |2017-01-13T09:00:42       |
|#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ssZ)}        |2017-01-13T09:00:42+0000  |
|#{formatDate(2017-01-13T09:00:42.862-05:00, yyyy-MM-dd'T'HH:mm:ssXXX)} |2017-01-13T09:00:42-05:00 |

Scenario: Check Hyperion expression #{formatDate}: only date
Meta:
    @testCaseId AAMH-3658
Then '#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd)}' is equal to '2017-01-13'

Scenario: Check Hyperion expression #{formatDate}: only time
Meta:
    @testCaseId AAMH-3659
Then '#{formatDate(2017-01-13T09:00:42.862Z, HH:mm:ss)}' is equal to '09:00:42'

Scenario: Check Hyperion expression #{formatDate}: change Time Zone
Meta:
    @testCaseId AAMH-3660
Then '<Expression>' is equal to '<Result>'
Examples:
|Expression                                                                            |Result                       |
|#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ss.SSSZ, -05:00)}           |2017-01-13T04:00:42.862-0500 |
|#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ss.SSSZ, -0500)}            |2017-01-13T04:00:42.862-0500 |
|#{formatDate(2017-01-13T04:00:42.862-05:00, yyyy-MM-dd'T'HH:mm:ss.SSSZ, GMT)}         |2017-01-13T09:00:42.862+0000 |
|#{formatDate(2017-01-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ss.SSSZ, America/New_York)} |2017-01-13T04:00:42.862-0500 |
|#{formatDate(2017-08-13T09:00:42.862Z, yyyy-MM-dd'T'HH:mm:ss.SSSZ, America/New_York)} |2017-08-13T05:00:42.862-0400 |
