Meta:
    @group hyperion-expressions
    @testSetId AAMH-5611

Scenario: Check Hyperion expression #{removeWrappingDoubleQuotes} from examples table
Meta:
    @testCaseId AAMH-3662
Then '#{removeWrappingDoubleQuotes(<jsonValue>)}' is equal to 'value'
Examples:
|jsonValue |
|"value"   |

Scenario: Check Hyperion expression #{removeWrappingDoubleQuotes} from step
Meta:
    @testCaseId AAMH-3663
Then '#{removeWrappingDoubleQuotes("value")}' is equal to 'value'
