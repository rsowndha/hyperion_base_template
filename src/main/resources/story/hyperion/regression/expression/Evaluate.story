Meta:
    @group hyperion-expressions
    @testSetId AAMH-28643

Scenario: Check Hyperion expression #{eval(expression to evaluate)}
Meta:
    @testCaseId AAMH-9175
Then '#{eval(<expression>)}' is equal to '<expectedResult>'
Examples:
|expression          |expectedResult|
|16 + 2 * 6          |28            |
|(16 + 2) * 6        |108           |
|100 / 5 - 16 * 2 + 6|-6            |