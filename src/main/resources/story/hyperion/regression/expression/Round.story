Meta:
    @group hyperion-expressions
    @testSetId AAMH-5593

Scenario: Check Hyperion expression #{round}: MaximumFractionDigits is zero
Meta:
    @testCaseId AAMH-3667
Then '#{round(<number>, 0)}' is equal to '<expectedNumber>'
Examples:
|number |expectedNumber |
|-5.9   |-6             |
|5.5    |6              |

Scenario: Check Hyperion expression #{round}: MaximumFractionDigits is less than default
Meta:
    @testCaseId AAMH-3668
Then '#{round(<number>, 1)}' is equal to '<expectedNumber>'
Examples:
|number |expectedNumber |
|-5.55  |-5.5           |
|5.51   |5.5            |

Scenario: Check Hyperion expression #{round}: MaximumFractionDigits is equal to default
Meta:
    @testCaseId AAMH-3669
Then '#{round(<number>, 2)}' is equal to '<expectedNumber>'
Examples:
|number |expectedNumber |
|-5.551 |-5.55          |
|5.559  |5.56           |

Scenario: Check Hyperion expression #{round}: default MaximumFractionDigits
Meta:
    @testCaseId AAMH-3670
Then '#{round(<number>)}' is equal to '<expectedNumber>'
Examples:
|number |expectedNumber |
|-5.559 |-5.56          |
|5.555  |5.56           |

Scenario: Check Hyperion expression #{round}: MaximumFractionDigits is more than default
Meta:
    @testCaseId AAMH-3671
Then '#{round(<number>, 3)}' is equal to '<expectedNumber>'
Examples:
|number  |expectedNumber |
|-5.5555 |-5.555         |
|5.5551  |5.555          |

Scenario: Check Hyperion expression #{round}: default MaximumFractionDigits - rounds to integer
Meta:
    @testCaseId AAMH-3672
Then '#{round(<number>)}' is equal to '<expectedNumber>'
Examples:
|number  |expectedNumber |
|-4.9999 |-5             |
|5.00000 |5              |
|5.0001  |5              |

Scenario: Check Hyperion expression #{round}: expression without MaximumFractionDigits parameter - long numbers
Meta:
    @testCaseId AAMH-3673
Then '#{round(<number>, 0)}' is equal to '<expectedNumber>'
Examples:
|number                                             |expectedNumber                                    |
|999999999999999999999999999999999999999999999999.9 |1000000000000000000000000000000000000000000000000 |
|9.999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999 |10|
