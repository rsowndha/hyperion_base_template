Meta:
    @group hyperion-expressions
    @testSetId AAMH-28642

Scenario: Check Hyperion expression #{toLowerCase}
Meta:
    @testCaseId AAMH-9801
Then '<expression>' is equal to '<result>'
Examples:
|expression          |result |
|#{toLowerCase(aBc)} |abc    |

Scenario: Check Hyperion expression #{toUpperCase}
Meta:
    @testCaseId AAMH-9802
Then '<expression>' is equal to '<result>'
Examples:
|expression          |result |
|#{toUpperCase(aBc)} |ABC    |

Scenario: Check Hyperion expression #{capitalize}
Meta:
    @testCaseId AAMH-9803
Then '<expression>' is equal to '<result>'
Examples:
|expression          |result |
|#{capitalize(aBc)}  |ABc    |

Scenario: Check Hyperion expression #{uncapitalize}
Meta:
    @testCaseId AAMH-9804
Then '<expression>' is equal to '<result>'
Examples:
|expression          |result |
|#{uncapitalize(ABc)}|aBc    |

Scenario: Check Hyperion nested expressions
Meta:
    @testCaseId AAMH-25507
Then '<expression>' is equal to '<result>'
Examples:
|expression                                                                         |result  |
|#{capitalize(#{trim(#{removeWrappingDoubleQuotes(#{toLowerCase(" HYPERION ")})})})}|Hyperion|

Scenario: Check Hyperion expression #{decodeFromBase64}
Meta:
    @testCaseId AAMH-28817
Then '<expression>' is equal to '<result>'
Examples:
|expression                                               |result                    |
|#{decodeFromBase64(eyJteUFjdXZ1ZUlkIjogIjZBMzBFMUYzIn0=)}|{"myAcuvueId": "6A30E1F3"}|