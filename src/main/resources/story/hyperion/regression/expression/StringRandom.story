Meta:
    @group hyperion-expressions
    @testSetId AAMH-5589

Scenario: Open main page
Meta:
    @testCaseId AAMH-3689
Given I am on the main application page

Scenario: Check Hyperion expression #{random}
Meta:
    @testCaseId AAMH-3690
When I enter '<expression>' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches '<verifyingRegex>'
Then '<expression>' is not equal to '<sameExpression>'
Examples:
|expression         |verifyingRegex |sameExpression     |
|#{random}          |[a-z0-9]{20}   |#{random}          |
|#{random()}        |[a-z0-9]{20}   |#{random()}        |
|#{random(A)}       |[a-z]{20}      |#{random(A)}       |
|#{random(N)}       |[\d]{20}       |#{random(N)}       |
|#{random(AN)}      |[a-z0-9]{20}   |#{random(AN)}      |
|#{random(U)}       |[A-Z0-9]{20}   |#{random(U)}       |
|#{random(L)}       |[a-z0-9]{20}   |#{random(L)}       |
|#{random(M)}       |[aA-zZ0-9]{20} |#{random(M)}       |
|#{random(10)}      |[a-z0-9]{10}   |#{random(10)}      |
|#{random(30)}      |[a-z0-9]{30}   |#{random(30)}      |
|#{random(A-U)}     |[A-Z]{20}      |#{random(A-U)}     |
|#{random(A-L)}     |[a-z]{20}      |#{random(A-L)}     |
|#{random(A-M)}     |[aA-zZ0-9]{20} |#{random(A-M)}     |
|#{random(A-10)}    |[a-z]{10}      |#{random(A-10)}    |
|#{random(N-U)}     |[\d]{20}       |#{random(N-U)}     |
|#{random(N-L)}     |[\d]{20}       |#{random(N-L)}     |
|#{random(N-M)}     |[\d]{20}       |#{random(N-M)}     |
|#{random(N-10)}    |[\d]{10}       |#{random(N-10)}    |
|#{random(AN-U)}    |[A-Z0-9]{20}   |#{random(AN-U)}    |
|#{random(AN-L)}    |[a-z0-9]{20}   |#{random(AN-L)}    |
|#{random(AN-M)}    |[aA-zZ0-9]{20} |#{random(AN-M)}    |
|#{random(AN-10)}   |[a-z0-9]{10}   |#{random(AN-10)}   |
|#{random(U-30)}    |[A-Z0-9]{30}   |#{random(U-30)}    |
|#{random(L-10)}    |[a-z0-9]{10}   |#{random(L-10)}    |
|#{random(M-30)}    |[aA-zZ0-9]{30} |#{random(M-30)}    |
|#{random(A-U-10)}  |[A-Z]{10}      |#{random(A-U-10)}  |
|#{random(A-L-10)}  |[a-z]{10}      |#{random(A-L-10)}  |
|#{random(A-M-20)}  |[aA-zZ]{20}    |#{random(A-M-20)}  |
|#{random(N-U-30)}  |[\d]{30}       |#{random(N-U-30)}  |
|#{random(N-L-30)}  |[\d]{30}       |#{random(N-L-30)}  |
|#{random(N-M-10)}  |[\d]{10}       |#{random(N-M-10)}  |
|#{random(AN-U-10)} |[A-Z0-9]{10}   |#{random(AN-U-10)} |
|#{random(AN-L-30)} |[a-z0-9]{30}   |#{random(AN-L-30)} |
|#{random(AN-M-20)} |[aA-zZ0-9]{20} |#{random(AN-M-20)} |
