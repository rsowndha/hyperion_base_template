Meta:
    @group hyperion-expressions
    @testSetId AAMH-5616

Lifecycle:
Examples:
|expectedResult |
|text           |

Scenario: Check Hyperion expression #{loadResource} from step
Meta:
    @testCaseId AAMH-3665
Then '#{loadResource(/story/hyperion/regression/expression/resource/resource.txt)}' is equal to '<expectedResult>'

Scenario: Check hyperion expression #{loadResource} from examples table
Meta:
    @testCaseId AAMH-3666
Then '<expression>' is equal to '<expectedResult>'
Examples:
|expression                                                                   |
|#{loadResource(/story/hyperion/regression/expression/resource/resource.txt)} |
