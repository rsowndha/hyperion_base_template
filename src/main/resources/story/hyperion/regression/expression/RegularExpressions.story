Meta:
    @group hyperion-expressions
    @testSetId AAMH-5621

Scenario: Check Hyperion expression #{replaceFirstByRegExp}
Meta:
    @testCaseId AAMH-3664
Then '<expression>' is equal to '<result>'
Examples:
|expression                                                                                   |result    |
|#{replaceFirstByRegExp(/node/(\d+)/edit, $1, /node/86/edit)}                                 |86        |
|#{replaceFirstByRegExp(/node/(\d+)/edit, $1/delete, /node/86/edit)}                          |86/delete |
|#{replaceFirstByRegExp(.*new password is (\d+).*, $1, """Updated, new password is 12345""")} |12345     |

Scenario: Check Hyperion expression #{replaceAllByRegExp}
Meta:
    @testCaseId AAMH-9805
Then '<expression>' is equal to '<result>'
Examples:
|expression                                              |result                   |
|#{replaceAllByRegExp(\s, -, convert spaces to dashes)}  |convert-spaces-to-dashes |

Scenario: Check Hyperion expression #{findFirstByRegExp}
Meta:
    @testCaseId AAMH-28194
Then '<expression>' is equal to '<result>'
Examples:
|expression                                          |result|
|#{findFirstByRegExp(te[a-z]+, this is test value)}  |test  |
