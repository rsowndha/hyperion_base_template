Meta:
    @group hyperion-expressions
    @testSetId AAMH-5587

Scenario: Check Hyperion expression #{formatDateTo}
Meta:
    @testCaseId AAMH-3661
Then '#{formatDateTo("2017-03-31T07:20:43.808Z", "yyyy-MM-dd'T'HH:mm:ss.SSSVV", EEE\, dd MMM yyyy HH:mm:ss ZZZZ)}' is equal to 'Fri, 31 Mar 2017 07:20:43 GMT'
Then '#{formatDateTo(Fri\, 31 Mar 2017 07:20:43 GMT, EEE\, dd MMM yyyy HH:mm:ss zzz, yyyy-MM-dd'T'HH:mm:ss)}' is equal to '2017-03-31T07:20:43'
