Meta:
    @group hyperion-expressions
    @testSetId AAMH-5631

Scenario: Open main page
Meta:
    @testCaseId AAMH-3679
Given I am on the main application page

Scenario: Check Hyperion expression #{story_random} from examples table with story name containing spaces
Meta:
    @testCaseId AAMH-3680
When I clear a field with the name 'message'
When I enter '<expression>' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches '<verifyingRegex>'
When I set the text found in search context to the 'story' variable 'result'
When I change context to the page
Then '${result}' is not equal to '<sameExpression>'
Examples:
|expression      |verifyingRegex          |sameExpression  |
|#{story_random} |space-story-[a-z0-9]{8} |#{story_random} |

Scenario: Check Hyperion expression #{story_indexed} from examples table with story name containing spaces
Meta:
    @testCaseId AAMH-3681
Then '<expression>' is equal to '<expectedResult>'
Examples:
|expression       |expectedResult |
|#{story_indexed} |space-story-1  |
|#{story_indexed} |space-story-2  |

Scenario: Check Hyperion expression #{story_random} from steps with story name containing spaces
Meta:
    @testCaseId AAMH-3682
When I clear a field with the name 'message'
When I enter '#{story_random}' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches 'space-story-[a-z0-9]{8}'
When I set the text found in search context to the 'story' variable 'result'
When I change context to the page
Then '${result}' is not equal to '#{story_random}'

Scenario: Check Hyperion expression #{story_indexed} from steps with story name containing spaces
Meta:
    @testCaseId AAMH-3683
Then '#{story_indexed}' is equal to 'space-story-3'
Then '#{story_indexed}' is equal to 'space-story-4'
