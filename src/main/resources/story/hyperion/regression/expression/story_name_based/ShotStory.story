Meta:
    @group hyperion-expressions
    @testSetId AAMH-5606

Scenario: Open main page
Meta:
    @testCaseId AAMH-3674
Given I am on the main application page

Scenario: Check Hyperion expression #{story_random} from examples table with shot story name
Meta:
    @testCaseId AAMH-3675
When I clear a field with the name 'message'
When I enter '<expression>' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches '<verifyingRegex>'
When I set the text found in search context to the 'story' variable 'result'
When I change context to the page
Then '${result}' is not equal to '<sameExpression>'
Examples:
|expression      |verifyingRegex         |sameExpression  |
|#{story_random} |shotstory-[a-z0-9]{10} |#{story_random} |

Scenario: Check Hyperion expression #{story_indexed} from examples table with shot story name
Meta:
    @testCaseId AAMH-3676
Then '<expression>' is equal to '<expectedResult>'
Examples:
|expression       |expectedResult |
|#{story_indexed} |shotstory-1    |
|#{story_indexed} |shotstory-2    |
|#{story_indexed} |shotstory-3    |

Scenario: Check Hyperion expression #{story_random} from steps with shot story name
Meta:
    @testCaseId AAMH-3677
When I clear a field with the name 'message'
When I enter '#{story_random}' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches 'shotstory-[a-z0-9]{10}'
When I set the text found in search context to the 'story' variable 'result'
When I change context to the page
Then '${result}' is not equal to '#{story_random}'

Scenario: Check Hyperion expression #{story_indexed} from steps with shot story name
Meta:
    @testCaseId AAMH-3678
Then '#{story_indexed}' is equal to 'shotstory-4'
Then '#{story_indexed}' is equal to 'shotstory-5'
