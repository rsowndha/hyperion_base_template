Meta:
    @group hyperion-expressions
    @testSetId AAMH-5633

Scenario: Open main page
Meta:
    @testCaseId AAMH-3684
Given I am on the main application page

Scenario: Check Hyperion expression #{story_random} from examples table table with long story name
Meta:
    @testCaseId AAMH-3685
When I clear a field with the name 'message'
When I enter '<expression>' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches '<verifyingRegex>'
When I set the text found in search context to the 'story' variable 'result'
When I change context to the page
Then '${result}' is not equal to '<sameExpression>'
Examples:
|expression      |verifyingRegex              |sameExpression  |
|#{story_random} |storyhasmoretha-[a-z0-9]{4} |#{story_random} |

Scenario: Check Hyperion expression #{story_indexed} from examples table with long story name
Meta:
    @testCaseId AAMH-3686
Then '<expression>' is equal to '<expectedResult>'
Examples:
|expression       |expectedResult       |
|#{story_indexed} |storyhasmorethantw-1 |
|#{story_indexed} |storyhasmorethantw-2 |
|#{story_indexed} |storyhasmorethantw-3 |
|#{story_indexed} |storyhasmorethantw-4 |
|#{story_indexed} |storyhasmorethantw-5 |
|#{story_indexed} |storyhasmorethantw-6 |
|#{story_indexed} |storyhasmorethantw-7 |
|#{story_indexed} |storyhasmorethantw-8 |
|#{story_indexed} |storyhasmorethantw-9 |
|#{story_indexed} |storyhasmorethant-10 |

Scenario: Check Hyperion expression #{story_random} from steps with long story name
Meta:
    @testCaseId AAMH-3687
When I clear a field with the name 'message'
When I enter '#{story_random}' in a field with the name 'message'
When I click on a button with the name 'addElement'
When I change context to an element with the attribute 'id'='inputText'
Then the text matches 'storyhasmoretha-[a-z0-9]{4}'
When I set the text found in search context to the 'story' variable 'result'
When I change context to the page
Then '${result}' is not equal to '#{story_random}'

Scenario: Check Hyperion expression #{story_indexed} from steps with long story name
Meta:
    @testCaseId AAMH-3688
Then '#{story_indexed}' is equal to 'storyhasmorethant-11'
Then '#{story_indexed}' is equal to 'storyhasmorethant-12'
