Meta:
    @group hyperion
    @testSetId AAMH-5622

Scenario: Check available URL was visited
Meta:
    @testCaseId AAMH-3637
When I send HTTP DELETE to the relative URL '/api/call-number?name=${counterName}'
When I crawl the site with URL '${mainPageURL}' and for each page do 
||
When I send HTTP GET to the relative URL '/api/call-number?name=${counterName}'
Then the response body is equal to '{"${counterName}":1}'
