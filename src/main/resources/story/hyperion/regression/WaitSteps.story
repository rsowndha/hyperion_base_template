Meta:
    @group plugin-web-ui
    @testSetId AAMH-5630

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification  When I wait until an element with the xpath '$elementXpath' appears/disappers
Meta:
    @testCaseId AAMH-3788
When I change context to an element with the attribute 'id'='disappear-element'
Then an element with the name '<imageName>' exists
When I click on a button with the name 'Hide/Show element'
When I wait until an element with the xpath '<imageXpath>' disappeares
Then an element with the name '<imageName>' does not exist
When I click on a button with the name 'Hide/Show element'
When I wait until an element with the xpath '<imageXpath>' appears
Then an element with the name '<imageName>' exists
Examples:
|imageName        |imageXpath|
|Rabbit in the hat|//div[@class='image-to-disappear']|

Scenario: Step verification When I wait until an element with the text '$text' disappears
Meta:
    @testCaseId AAMH-3789
When I change context to an element with the attribute 'id'='disappear-element'
Then an element with the name '<imageName>' exists
When I click on a button with the name 'Hide/Show element'
And I wait until an element with the text '<imageName>' disappears
Then an element with the name '<imageName>' does not exist
Examples:
|imageName        |
|image-to-disappear|

Scenario: Step verification When I wait until an element with the text '$text' appears
Meta:
    @testCaseId AAMH-3790
When I change context to an element with the attribute 'id'='disappear-element'
Then an element with the name '<imageName>' does not exist
When I click on a button with the name 'Hide/Show element'
When I wait until an element with the text '<imageName>' appears
Then an element with the name '<imageName>' exists
Examples:
|imageName        |
|Rabbit in the hat|

Scenario: Step verification When I wait until an element with the name '$elementName' disappears
Meta:
    @testCaseId AAMH-3791
When I change context to an element with the attribute 'id'='disappear-element'
Then an element with the name '<imageName>' exists
When I click on a button with the name 'Hide/Show element'
When I wait until an element with the name '<imageName>' disappears
Then an element with the name '<imageName>' does not exist
Examples:
|imageName         |
|image-to-disappear|

Scenario: Step verification  When I wait until an element with the name '$elementName' appears
Meta:
    @testCaseId AAMH-3792
When I change context to an element with the attribute 'id'='disappear-element'
Then an element with the name '<imageName>' does not exist
When I click on a button with the name 'Hide/Show element'
When I wait until an element with the name '<imageName>' appears
Then an element with the name '<imageName>' exists
Examples:
|imageName         |
|image-to-disappear|

Scenario: Step verification When I wait until elements with the name '$elementName' appear
Meta:
    @testCaseId AAMH-3793
When I change context to an element with the attribute 'id'='disappear-element'
Then an element with the name '<imageName>' exists
When I click on a button with the name 'Hide/Show element'
And I wait until an element with the text '<imageName>' disappears
Then an element with the name '<imageName>' does not exist
When I click on a button with the name 'Hide/Show element'
When I wait until elements with the name '<imageName>' appear
Then at least one element with the attribute 'class'='image-to-disappear' exists
Examples:
|imageName         |
|image-to-disappear|

Scenario: Step verification When I wait until a frame with the name '$frameName' appears
Meta:
    @testCaseId AAMH-3794
When I change context to an element with the attribute 'id'='disappear-frame'
Then a frame with the attribute 'name'='<frameName>' exists
When I click on a button with the name 'Hide/Show frame'
When I wait until an element with the text 'frame-to-disappear' disappears
Then an element with the name '<frameName>' does not exist
When I click on a button with the name 'Hide/Show frame'
When I change context to the page
When I wait until a frame with the name '<frameName>' appears
Then a frame with the attribute 'src'='/html/frame.html' exists
Examples:
|frameName        |
|frame-with-name  |

Scenario: Step verification When I wait until the page title contains the text '$text'
Meta:
    @testCaseId AAMH-3795
Then the page title is 'Hyperion Template'
When I click on an element with the text 'This is a link'
When I wait until the page title contains the text 'Other Page'
Then the page title is 'Hyperion Template - Other Page'

Scenario: Step verification When I wait '$duration' with '$pollingDuration' polling until an element located by $locator appears
Meta:
    @testCaseId AAMH-32968
When I click on an element by the xpath '//ul[@class='nav navbar-nav']/li/a[text()='Page Other']'
When I click on a button with the name 'Toggle element visibility with delay'
When I wait 'PT3S' with 'PT0.5S' polling until an element located by By.xpath(//div[@id='delayed']) appears
Then an element by the xpath '//div[@id='delayed']' exists
