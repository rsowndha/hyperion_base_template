Meta:
    @group hyperion
    @testSetId AAMH-28641

Scenario: Verification NEXT_BATCHES variable is available in next batch
Meta:
    @testCaseId AAMH-28623
Then '${NEXT_BATCHES_VAR}' is EQUAL_TO 'nextBatchesValue'