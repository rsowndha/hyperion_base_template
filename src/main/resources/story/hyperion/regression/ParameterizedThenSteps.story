Meta:
    @group plugin-web-ui
    @testSetId AAMH-5597

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Number of radio buttons with specified parameters found
Meta:
    @testCaseId AAMH-3759
Then the number of radio buttons with parameters is equal to '1':
|name  |state|
|Male  |VISIBLE|

Scenario: Number of elements with specified parameters found
Meta:
    @testCaseId AAMH-3760
Then the number of elements with parameters is equal to '3':
|xpath				   |name  |visibility |state  |
|//input[@type='radio']|gender|true       |ENABLED|
Then the number of elements with parameters is greater than or equal to '1': 
|tagName|visibility|src			     |alt     |
|img    |true	   |/img/hyperion.jpg|Hyperion|

Scenario: Number of carousels with specified parameters found
Meta:
    @testCaseId AAMH-3761
Then the number of carousels with parameters is equal to '1':
|state  |slidesQuantity|
|VISIBLE|4             |

Scenario: Number of buttons with specified parameters found
Meta:
    @testCaseId AAMH-3762
Then the number of buttons with parameters is equal to 1: 
|name              |tooltip	 |imageSrc         |
|button-with-image |MyToolTip|/img/hyperion.jpg|

Scenario: Number of frames with specified parameters found
Meta:
    @testCaseId AAMH-3763
Then the number of frames with parameters is greater than or equal to '1': 
|state  |attributeName|attributeValue|
|VISIBLE|name         |iframe        |

Scenario: Number of images with specified parameters found
Meta:
    @testCaseId AAMH-3764
Then the number of images with parameters is equal to '2': 
|imageSrc              |
|/img/hyperion.jpg     |

Scenario: Number of scrollbars with specified parameters found
Meta:
    @testCaseId AAMH-3765
Then the number of scrollbars is equal to '1'

Scenario: Number of of drop downs with specified parameters found
Meta:
    @testCaseId AAMH-3766
Then the number of drop downs with parameters is equal to '1': 
|dropDownName|state  |
|cars        |ENABLED|

Scenario: Number of checkboxes with specified parameters found
Meta:
    @testCaseId AAMH-3767
Then the number of checkboxes with parameters is equal to '1': 
|attributeType|attributeValue|
|id           |car           |

Scenario: Number of links with specified parameters found
Meta:
    @testCaseId AAMH-3768
Then the number of links with parameters is equal to '1': 
|text          |state  |URL						|
|This is a link|VISIBLE|/html/page.html         |

Scenario: Number of script elements with specified parameters found
Meta:
    @testCaseId AAMH-3769
Then the number of script elements with parameters is equal to '1': 
|srcPart                        |
|/lib/jquery/jquery.min.js      |

Scenario: Number of fields with specified parameters found
Meta:
    @testCaseId AAMH-3770
Then the number of fields with parameters is equal to '1': 
|fieldName                 |placeholder               |
|placeholderArea           |This is placeholder text. |
