Meta:
    @group plugin-web-ui
    @testSetId AAMH-5591

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification that an image exists or does not exist
Meta:
    @testCaseId AAMH-3701
When I change context to an element by the xpath '//button[@title="MyToolTip"]'
Then an image with the src '<srcImg>' exists
Then a [VISIBLE] image with the src '<srcImg>' exists
Then an image with the src '/img/wrong.jpg' does not exist
Then an image with the src containing '<srcPart>' exists
Then a [VISIBLE] image with the src containing '<srcPart>' exists
Examples:
|srcImg             |srcPart  |
|/img/hyperion.jpg  |hyperion |

Scenario: Step verification When I hover a mouse over an image with the src
Meta:
    @testCaseId AAMH-3703
When I go to the relative URL '/html/page.html'
When I change context to an element by the xpath '//div[@id="img-for-hover"]'
Then an element with the name '<messageElement>' does not exist
When I hover a mouse over an image with the src '/img/hyperion.jpg'
Then an element with the name '<messageElement>' exists
Examples:
|messageElement             |
|You hover over an image!   |

Scenario: Step verification When I click on an image with the src
Meta:
    @testCaseId AAMH-3704
Then the page title is 'Hyperion Template - Other Page'
When I change context to an element by the xpath '//div[@id="img-for-hover"]'
When I click on an image with the src '/img/hyperion.jpg'
Then the page title is 'Hyperion Template'

Scenario: Step verification When I hover a mouse over an image with the tooltip '$tooltipImage'
Meta:
    @testCaseId AAMH-3706
When I go to the relative URL '/html/page.html'
When I change context to an element by the xpath '//div[@id="img-for-hover"]'
Then an element with the name '<messageElement>' does not exist
When I hover a mouse over an image with the tooltip 'ImageToolTip'
Then an element with the name '<messageElement>' exists
Examples:
|messageElement             |
|You hover over an image!   |

Scenario: Step verification When I click on an image with the name '$imageName'
Meta:
    @testCaseId AAMH-3707
Then the page title is 'Hyperion Template - Other Page'
When I change context to an element by the xpath '//div[@id="img-for-hover"]'
When I click on an image with the name 'ImageWithTooltip'
Then the page title is 'Hyperion Template'

Scenario: Step verification that an image with the tooltip exists
Meta:
    @testCaseId AAMH-3709
When I go to the relative URL '/html/page.html'
When I change context to an element by the xpath '//div[@id="img-for-hover"]'
Then an image with the tooltip '<tooltipImage>' and src containing 'hyperion' exists
Then an image with the src '<srcImage>' and tooltip '<tooltipImage>' exists
Then an image with the tooltip '<tooltipImage>' exists
Examples:
|tooltipImage      |srcImage            |
|ImageToolTip      |/img/hyperion.jpg   |

Scenario: Step verification that a 'state' image with the tooltip exists
Meta:
    @testCaseId AAMH-3710
When I change context to an element by the xpath '//div[@id="img-for-hover"]'
Then a [VISIBLE] image with the src '<srcImage>' and tooltip '<tooltipImage>' exists
Then a [VISIBLE] image with the tooltip '<tooltipImage>' exists
Examples:
|tooltipImage      |srcImage            |
|ImageToolTip      |/img/hyperion.jpg   |
