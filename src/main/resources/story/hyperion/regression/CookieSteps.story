Meta:
    @group plugin-web-ui
    @testSetId AAMH-5610

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I remove all cookies from the current domain
Meta:
    @testCaseId AAMH-3635
When I create a cookie
When I remove all cookies from the current domain
Then a cookie with the name 'cookie' is not set

Scenario: Step verification When I remove a cookie with the name '$cookieName' from the current domain
Meta:
    @testCaseId AAMH-3636
When I go to the relative URL '/'
When I create a cookie
When I remove a cookie with the name 'cookie' from the current domain
Then a cookie with the name 'cookie' is not set

Scenario: Step verification Then the number of cookies with the value that $validationRule '$text' $comparisonRule '$quantity'
Meta:
    @testCaseId AAMH-32983
When I go to the relative URL '/'
When I set all cookies for current domain:
|cookieName   |cookieValue|path|
|cookie       |value      |/   |
|anotherCookie|value      |/   |
When I refresh the page
Then the number of cookies with the value that is equal to 'value' = '2'
