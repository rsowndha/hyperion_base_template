Meta:
    @testSetId AAMH-28639

GivenStories: story/hyperion/regression/precondition/GivenStoryWithAllLevelExamples.story,
			  story/hyperion/regression/precondition/SimpleGivenStoryWithScenarioExample.story

Lifecycle:
Examples:
|lifecycleLine|number    |
|1            |1         |
|2            |2         |

Scenario: Simple scenario
Meta:
    @testCaseId AAMH-23523; AAMH-9228
Then '<lifecycleLine>' is equal to '<number>'