Meta:
    @group bdd-engine
    @testSetId AAMH-6465

Scenario: Examples table rows are cut off
Meta:
    @testCaseId AAMH-6464
Then 'value' is equal to '<var>'
Examples:
table:
|var          |
|value        |
|should be cut|
parameters: var
top_rows: 1
