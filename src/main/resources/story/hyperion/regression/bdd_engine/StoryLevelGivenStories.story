Meta:
    @group bdd-engine
    @testSetId AAMH-5629

GivenStories: story/hyperion/regression/precondition/GivenStoryUsingExamplesFromBase.story#{0},
              story/hyperion/regression/precondition/GivenStoryUsingExamplesFromBase.story#{1}

Lifecycle:
Examples:
|actual      |expected    |
|some data   |some data   |
|another data|another data|

Scenario: Input data is passed from this story to GivenStory
Meta:
    @testCaseId AAMH-3607; AAMH-14052
Then '<expected>' is equal to '<actual>'
