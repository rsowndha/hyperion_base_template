Meta:
    @group bdd-engine
    @testSetId AAMH-5632

GivenStories: story/hyperion/regression/precondition/GivenStoryWithAllLevelExamples.story

Scenario: simple scenario to be executed after given story
Meta:
    @testCaseId AAMH-3606
Then 'data' is equal to 'data'
