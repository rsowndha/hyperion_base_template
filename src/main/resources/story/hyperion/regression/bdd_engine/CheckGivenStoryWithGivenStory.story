Meta:
    @group bdd-engine
    @testSetId AAMH-5623

GivenStories: story/hyperion/regression/precondition/GivenStoryWithGivenStory.story#{0}

Lifecycle:
Examples:
|actual      |expected    |
|some data   |some data   |

Scenario: Input data is passed from this story to GivenStory
Meta:
    @testCaseId AAMH-3603
Then '<expected>' is equal to '<actual>'
