Meta:
    @group bdd-engine
    @testSetId AAMH-5604

Lifecycle:
Examples:
|actual      |expected    |
|some data   |some data   |
|another data|another data|

Scenario: Scenario-level GivenStories with story-level Examples
Meta:
    @testCaseId AAMH-3604; AAMH-14051
GivenStories: story/hyperion/regression/precondition/GivenStoryUsingExamplesFromBase.story
Then '<expected>' is equal to '<actual>'
