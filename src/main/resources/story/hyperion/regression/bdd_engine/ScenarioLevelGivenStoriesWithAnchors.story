Meta:
    @group bdd-engine
    @testSetId AAMH-5608

Scenario: Scenario-level GivenStories with anchor meta filter
Meta:
    @testCaseId AAMH-4976
GivenStories: story/hyperion/regression/precondition/GivenStoryForAnchors.story#{id:given1}
Then '<expected>' is equal to '<actual>'
Examples:
|actual    |expected  |
|some data |some data |

Scenario: Scenario-level GivenStories with anchor examples table row
Meta:
    @testCaseId AAMH-4977
GivenStories: story/hyperion/regression/precondition/GivenStoryForAnchors.story#{1}
Then '<examples_shouldn't_pass_here_if_examples_anchor_exists>' is equal to '<examples_shouldn't_pass_here_if_examples_anchor_exists>'
Examples:
|second_actual |second_expected |actual      |expected  |
|another data  |same data       |catz data   |doge data |
|some data     |some data       |some data   |some data |
