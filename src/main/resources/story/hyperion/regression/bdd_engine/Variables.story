Meta:
    @group bdd-engine
    @testSetId AAMH-5588

Scenario: ASCII global variable is loaded from properties file
Meta:
    @testCaseId AAMH-3609
Then 'value' is equal to '${asciiVariable}'

Scenario: UTF-8 global variable is loaded from properties file
Meta:
    @testCaseId AAMH-3610
Then '用' is equal to '${utf8Variable}'

Scenario: UTF-8 escaped global variable is loaded from properties file
Meta:
    @testCaseId AAMH-3611
Then '用' is equal to '${utf8EscapedVariable}'
