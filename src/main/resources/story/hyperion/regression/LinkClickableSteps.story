Meta:
    @group plugin-web-integration
    @testSetId AAMH-5625
					 
Scenario: Step verification Then all links are clickable
Meta:
    @testCaseId AAMH-3726
Given I am on the main application page
When I send HTTP DELETE to the relative URL '/api/call-number?name=pageCallNumber'
When I change context to an element with the name 'clickable-links'
Then all links are clickable
When I send HTTP GET to the relative URL '/api/call-number?name=pageCallNumber'
Then the response body is equal to '{"pageCallNumber":1}'
