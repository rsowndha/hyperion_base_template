Meta:
    @group plugin-web-ui

GivenStories: story/hyperion/regression/precondition/OpenMainApplicationPage.story

Scenario: Step verification When I confirm an action in an alert with the message
!-- Skipped due to jbrowser bug https://github.com/MachinePublishers/jBrowserDriver/issues/147
Meta:
@skip
Then an element with the name '<messageElement>' does not exist
When I click on a button with the name 'Click Me!'
When I wait until an alert appears
Then an alert is present
When I confirm an action in an alert with the message 'Hello World!'
When I wait until an alert disappears
Then an alert is not present
Then an element with the name '<messageElement>' exists
Examples:
|messageElement          |
|You pressed OK!         |

Scenario: Step verification When I confirm an action in an alert with the message containing
!-- Skipped due to jbrowser bug https://github.com/MachinePublishers/jBrowserDriver/issues/147
Meta:
@skip
When I click on a button with the name 'Click Me!'
When I wait until an alert appears
When I confirm an action in an alert with the message containing 'Hello'
When I wait until an alert disappears

Scenario: Step verification When I decline an action in an alert with the message
!-- Skipped due to jbrowser bug https://github.com/MachinePublishers/jBrowserDriver/issues/147
Meta:
@skip
Then an element with the name '<messageElement>' does not exist
When I click on a button with the name 'Click Me!'
When I wait until an alert appears
When I decline an action in an alert with the message 'Hello World!'
When I wait until an alert disappears
Then an element with the name '<messageElement>' exists
Examples:
|messageElement          |
|You pressed Cancel!     |
