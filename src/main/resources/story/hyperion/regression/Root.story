Meta:
    @rootStory
    @testSetId AAMH-28644

GivenStories: /story/hyperion/regression/precondition/GivenStory3.story#{0}

Lifecycle:
Examples:
|rootKey  |overridenKey|
|rootValue|rootStory   |

Scenario: Root story scenario
Meta:
	@root
    @testCaseId AAMH-20345

GivenStories: /story/hyperion/regression/precondition/GivenStory1.story

Then '<overridenKey>' is EQUAL_TO 'rootStory'
Examples:
|scenarioKey       |
|scenarioRootValue1|
|scenarioRootValue2|