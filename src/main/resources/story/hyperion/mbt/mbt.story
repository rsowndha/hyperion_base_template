Scenario: Open_main_application_page
Meta:
	@edge
Given I am on the main application page

Scenario: Open_page
Meta:
	@edge
When I go to the relative URL '/html/page.html'

Scenario: Open_other_page
Meta:
	@edge
When I go to the relative URL '/html/pageOther.html'

Scenario: Homepage_is_opened
Meta:
	@vertex
Then the page title is 'Hyperion Template'
Then the page has the relative URL '/'

Scenario: Page_is_opened
Meta:
	@vertex
Then the page title is 'Hyperion Template - Other Page'
Then the page has the relative URL '/html/page.html'

Scenario: Other_page_is_opened
Meta:
	@vertex
Then the page title is 'Hyperion Template - Other Page'
Then the page has the relative URL '/html/pageOther.html'