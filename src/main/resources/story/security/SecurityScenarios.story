Meta:
    @group Security scenarios

Scenario: Application Configuration. Access to website/web application configuration files (e.g., web.config, web.xml, config.php, etc.)
shall be based on a least privilege and business need-to-know basis.
When I issue a HTTP GET request for a resource with the URL '${mainApplicationPageUrl}/<configFile>'
Then the response code is equal to '403'
Examples:
|configFile|
|engine.sql|
|engine.theme|
|engine.tpl|
|inc.theme|
|inc.tpl|
|inc.sql|
|info.sql|

Scenario: System Configuration. HTTP methods not used by the website/web application (e.g., TRACE, PUT, DELETE, OPTIONS, etc.) shall be disabled.
When I issue a HTTP OPTIONS request for a resource with the URL '${mainApplicationPageUrl}'
Then unused http methods are restricted

Scenario: Cookie and Session Management. Session cookies shall be enabled instead of persistent cookies, when possible.
If persistent cookies are enabled, the "Max-Age" attribute shall be set to an expiration date.
When I issue a HTTP GET request for a resource with the URL '${mainApplicationPageUrl}'
Then response header 'Cache-Control' contains attribute: 
|attribute|
|max-age|

Scenario: Input validation. Search by text with length more than max
Meta:
    @layout desktop
Given I am on the main application page
When I perform search by the text '<text>'
Then the text 'Search cannot be longer than 128 characters but is currently 275 characters long.' exists
When I go to the relative URL '<relativeURL>'
Then the text 'Search cannot be longer than 128 characters but is currently 275 characters long.' exists
Examples:
story/security/InputValidation_Length.table

Scenario: Input validation. Add SQL injection to site url
Meta:
    @layout desktop
When I go to the relative URL '<sqlInjection>'
Then the text 'Page not found' exists
Examples:
story/security/InputValidation_SQL_Injections.table

Scenario: Input validation. Search with XSS injection via Search input and url
Meta:
    @layout desktop
When I perform search by the text '<xssInjection>'
Then the text 'Search Results' exists
When I go to the relative URL '<xssInjectionEncoded>'
Then the text 'Search Results' exists
Examples:
story/security/InputValidation_XSS_Injections.table

Scenario: Input validation. Add Server Code, XXE, File Inclusion and other injections to site url
Meta:
    @layout desktop
When I go to the relative URL '<injection>'
Then the text 'Search Results' exists
Examples:
story/security/InputValidation_OtherInjections.table
